package gmm

import (
	"bytes"
	"crypto/hmac"
	cryptoRand "crypto/rand"
	"crypto/sha256"
	"encoding/hex"
	"errors"
	"fmt"
	"math/big"
	"reflect"
	"strconv"
	"strings"
	"time"
	"unsafe"

	"github.com/antihax/optional"
	"github.com/mitchellh/mapstructure"
	"github.com/mohae/deepcopy"
	"go.mongodb.org/mongo-driver/bson"

	"gitee.com/liu-zhao234568/amf/consumer"
	"gitee.com/liu-zhao234568/amf/context"
	gmm_message "gitee.com/liu-zhao234568/amf/gmm/message"
	ngap_message "gitee.com/liu-zhao234568/amf/ngap/message"
	"gitee.com/liu-zhao234568/amf/producer/callback"
	"gitee.com/liu-zhao234568/amf/util"
	"github.com/free5gc/fsm"
	"github.com/free5gc/nas"
	"github.com/free5gc/nas/nasConvert"
	"github.com/free5gc/nas/nasMessage"
	"github.com/free5gc/nas/nasType"
	"github.com/free5gc/nas/security"
	"github.com/free5gc/ngap/ngapConvert"
	"github.com/free5gc/ngap/ngapType"
	"github.com/free5gc/openapi/Nnrf_NFDiscovery"
	"github.com/free5gc/openapi/models"

	"github.com/free5gc/util_3gpp/suci"

	ausf_logger "github.com/free5gc/ausf/logger"

	smf_context "gitee.com/liu-zhao234568/smf/context"
	smf_logger "gitee.com/liu-zhao234568/smf/logger"
	smf_producer "gitee.com/liu-zhao234568/smf/producer"

	pcf_context "github.com/free5gc/pcf/context"
	pcf_logger "github.com/free5gc/pcf/logger"
	pcf_util "github.com/free5gc/pcf/util"

	"github.com/free5gc/UeauCommon"
	"github.com/free5gc/milenage"

	"io/ioutil"

	ethcrypto "gitee.com/liu-zhao234568/cntest/crypto"
	//"gopkg.in/yaml.v2"

	"os"

	"gitee.com/liu-zhao234568/cntest/accounts/keystore"
	"gitee.com/liu-zhao234568/cntest/common"

	"log"

	"gitee.com/liu-zhao234568/amf/gmm/jianquan"

	"gitee.com/liu-zhao234568/cntest/accounts/abi/bind"
	"gitee.com/liu-zhao234568/cntest/ethclient"
)

// By uuuuu - entry

// UE - Kseaf map
var Kseaf map[string][]byte

// UE - xres map
var xresForConfirm map[string]string

// UE - SQN Generator
type sqnGen struct{
	rand []byte
	opc []byte
	k []byte
}

// UE - SQN map
var mapForSqn map[string]*sqnGen

// 下面是 区块链 初始化部分
var DefaultCurve = ethcrypto.S256()
var saddr string
var ks *keystore.KeyStore

type Conf struct {
	Ssk1 string
}

const (
	// number of bits in a big.Word
	wordBits = 32 << (uint64(^big.Word(0)) >> 63)
	// number of bytes in a big.Word
	wordBytes = wordBits / 8
)

const (
	SqnMAx    int64 = 0x7FFFFFFFFFF
	ind       int64 = 32
	keyLen    int   = 16
	opStrLen  int   = 32
	opcStrLen int   = 32
)

const (
	authenticationRejected string = "AUTHENTICATION_REJECTED"
)

var (
	client *ethclient.Client
)

func init() {
	//keystore
	ks = keystore.NewKeyStore("./eth_contract_address/tmp", keystore.StandardScryptN, keystore.StandardScryptP)
	//获取连接与eth客户端//client, _ = ethclient.Dial("http://localhost:8545")
	ip_port_byte, _ := ioutil.ReadFile("./nginxaddr.txt")
	ip_port := (*string)(unsafe.Pointer(&ip_port_byte))
	client, _ = ethclient.Dial(*ip_port)
	if client == nil {
		fmt.Println("+++Dial err+++")
		//panic("连接错误")
		return
	}

	// UE - Kseaf map
	Kseaf = make(map[string][]byte)
	// UE - SQN map
	mapForSqn = make(map[string]*sqnGen)

	// UE - xres map
	xresForConfirm = make(map[string]string)
}

func xor(one, other []byte) (xor []byte) {
	xor = make([]byte, len(one))
	for i := 0; i < len(one); i++ {
		xor[i] = one[i] ^ other[i]
	}
	return xor

}

// By uuuuu - end

func strictHex(s string, n int) string {
	l := len(s)
	if l < n {
		return fmt.Sprintf(strings.Repeat("0", n-l) + s)
	} else {
		return s[l-n : l]
	}
}

func eapAkaPrimePrf(ikPrime string, ckPrime string, identity string) (string, string, string, string, string) {
	keyAp := ikPrime + ckPrime

	var key []byte
	if keyTmp, err := hex.DecodeString(keyAp); err != nil {
		ausf_logger.EapAuthComfirmLog.Warnf("Decode key AP failed: %+v", err)
	} else {
		key = keyTmp
	}
	sBase := []byte("EAP-AKA'" + identity)

	MK := ""
	prev := []byte("")
	//_ = prev
	prfRounds := 208/32 + 1
	for i := 0; i < prfRounds; i++ {
		// Create a new HMAC by defining the hash type and the key (as byte array)
		h := hmac.New(sha256.New, key)

		hexNum := string(i + 1)
		ap := append(sBase, hexNum...)
		s := append(prev, ap...)

		// Write Data to it
		if _, err := h.Write(s); err != nil {
			ausf_logger.EapAuthComfirmLog.Errorln(err.Error())
		}

		// Get result and encode as hexadecimal string
		sha := string(h.Sum(nil))
		MK += sha
		prev = []byte(sha)
	}

	K_encr := MK[0:16]  // 0..127
	K_aut := MK[16:48]  // 128..383
	K_re := MK[48:80]   // 384..639
	MSK := MK[80:144]   // 640..1151
	EMSK := MK[144:208] // 1152..1663
	return K_encr, K_aut, K_re, MSK, EMSK
}

func HandleULNASTransport(ue *context.AmfUe, anType models.AccessType,
	ulNasTransport *nasMessage.ULNASTransport) error {
	ue.GmmLog.Infoln("Handle UL NAS Transport")

	if ue.MacFailed {
		return fmt.Errorf("NAS message integrity check failed")
	}

	switch ulNasTransport.GetPayloadContainerType() {
	// TS 24.501 5.4.5.2.3 case a)
	case nasMessage.PayloadContainerTypeN1SMInfo:
		return transport5GSMMessage(ue, anType, ulNasTransport)
	case nasMessage.PayloadContainerTypeSMS:
		return fmt.Errorf("PayloadContainerTypeSMS has not been implemented yet in UL NAS TRANSPORT")
	case nasMessage.PayloadContainerTypeLPP:
		return fmt.Errorf("PayloadContainerTypeLPP has not been implemented yet in UL NAS TRANSPORT")
	case nasMessage.PayloadContainerTypeSOR:
		return fmt.Errorf("PayloadContainerTypeSOR has not been implemented yet in UL NAS TRANSPORT")
	case nasMessage.PayloadContainerTypeUEPolicy:
		ue.GmmLog.Infoln("AMF Transfer UEPolicy To PCF")
		callback.SendN1MessageNotify(ue, models.N1MessageClass_UPDP,
			ulNasTransport.PayloadContainer.GetPayloadContainerContents(), nil)
	case nasMessage.PayloadContainerTypeUEParameterUpdate:
		ue.GmmLog.Infoln("AMF Transfer UEParameterUpdate To UDM")
		upuMac, err := nasConvert.UpuAckToModels(ulNasTransport.PayloadContainer.GetPayloadContainerContents())
		if err != nil {
			return err
		}
		err = consumer.PutUpuAck(ue, upuMac)
		if err != nil {
			return err
		}
		ue.GmmLog.Debugf("UpuMac[%s] in UPU ACK NAS Msg", upuMac)
	case nasMessage.PayloadContainerTypeMultiplePayload:
		return fmt.Errorf("PayloadContainerTypeMultiplePayload has not been implemented yet in UL NAS TRANSPORT")
	}
	return nil
}

func buildCreateSmContextRequest(ue *context.AmfUe, smContext *context.SmContext,
	requestType *models.RequestType) (smContextCreateData models.SmContextCreateData) {
	context := context.AMF_Self()
	smContextCreateData.Supi = ue.Supi
	smContextCreateData.UnauthenticatedSupi = ue.UnauthenticatedSupi
	smContextCreateData.Pei = ue.Pei
	smContextCreateData.Gpsi = ue.Gpsi
	smContextCreateData.PduSessionId = smContext.PduSessionID()
	snssai := smContext.Snssai()
	smContextCreateData.SNssai = &snssai
	smContextCreateData.Dnn = smContext.Dnn()
	smContextCreateData.ServingNfId = context.NfId
	smContextCreateData.Guami = &context.ServedGuamiList[0]
	smContextCreateData.ServingNetwork = context.ServedGuamiList[0].PlmnId
	if requestType != nil {
		smContextCreateData.RequestType = *requestType
	}
	smContextCreateData.N1SmMsg = new(models.RefToBinaryData)
	smContextCreateData.N1SmMsg.ContentId = "n1SmMsg"
	smContextCreateData.AnType = smContext.AccessType()
	if ue.RatType != "" {
		smContextCreateData.RatType = ue.RatType
	}
	// TODO: location is used in roaming scenerio
	// if ue.Location != nil {
	// 	smContextCreateData.UeLocation = ue.Location
	// }
	smContextCreateData.UeTimeZone = ue.TimeZone
	smContextCreateData.SmContextStatusUri = context.GetIPv4Uri() + "/namf-callback/v1/smContextStatus/" +
		ue.Guti + "/" + strconv.Itoa(int(smContext.PduSessionID()))

	return smContextCreateData
}

func transport5GSMMessage(ue *context.AmfUe, anType models.AccessType,
	ulNasTransport *nasMessage.ULNASTransport) error {
	var pduSessionID int32

	ue.GmmLog.Info("Transport 5GSM Message to SMF")

	smMessage := ulNasTransport.PayloadContainer.GetPayloadContainerContents()

	if id := ulNasTransport.PduSessionID2Value; id != nil {
		pduSessionID = int32(id.GetPduSessionID2Value())
	} else {
		return errors.New("PDU Session ID is nil")
	}

	// case 1): looks up a PDU session routing context for the UE and the PDU session ID IE in case the Old PDU
	// session ID IE is not included
	if ulNasTransport.OldPDUSessionID == nil {
		smContext, smContextExist := ue.SmContextFindByPDUSessionID(pduSessionID)
		requestType := ulNasTransport.RequestType

		if requestType != nil {
			switch requestType.GetRequestTypeValue() {
			case nasMessage.ULNASTransportRequestTypeInitialEmergencyRequest:
				fallthrough
			case nasMessage.ULNASTransportRequestTypeExistingEmergencyPduSession:
				ue.GmmLog.Warnf("Emergency PDU Session is not supported")
				gmm_message.SendDLNASTransport(ue.RanUe[anType], nasMessage.PayloadContainerTypeN1SMInfo,
					smMessage, pduSessionID, nasMessage.Cause5GMMPayloadWasNotForwarded, nil, 0)
				return nil
			}
		}

		// By uuuuu - entry
		// 下面这一段 if smContextExist 没有修改，仍是 向 SMF 请求。改动的是 else，即 会话新建。

		// AMF has a PDU session routing context for the PDU session ID and the UE
		if smContextExist {
			// case i) Request type IE is either not included
			if requestType == nil {
				return forward5GSMMessageToSMF(ue, anType, pduSessionID, smContext, smMessage)
			}

			switch requestType.GetRequestTypeValue() {
			case nasMessage.ULNASTransportRequestTypeInitialRequest:
				smContext.StoreULNASTransport(ulNasTransport)
				//  perform a local release of the PDU session identified by the PDU session ID and shall request
				// the SMF to perform a local release of the PDU session
				updateData := models.SmContextUpdateData{
					Release: true,
					Cause:   models.Cause_REL_DUE_TO_DUPLICATE_SESSION_ID,
					SmContextStatusUri: fmt.Sprintf("%s/namf-callback/v1/smContextStatus/%s/%d",
						ue.ServingAMF().GetIPv4Uri(), ue.Guti, pduSessionID),
				}
				ue.GmmLog.Warningf("Duplicated PDU session ID[%d]", pduSessionID)
				smContext.SetDuplicatedPduSessionID(true)
				response, _, _, err := consumer.SendUpdateSmContextRequest(smContext, updateData, nil, nil)
				if err != nil {
					return err
				} else if response == nil {
					err := fmt.Errorf("PDU Session ID[%d] can't be released in DUPLICATE_SESSION_ID case", pduSessionID)
					ue.GmmLog.Errorln(err)
					gmm_message.SendDLNASTransport(ue.RanUe[anType], nasMessage.PayloadContainerTypeN1SMInfo,
						smMessage, pduSessionID, nasMessage.Cause5GMMPayloadWasNotForwarded, nil, 0)
				} else if response != nil {
					smContext.SetUserLocation(ue.Location)
					responseData := response.JsonData
					n2Info := response.BinaryDataN2SmInformation
					if n2Info != nil {
						switch responseData.N2SmInfoType {
						case models.N2SmInfoType_PDU_RES_REL_CMD:
							ue.GmmLog.Debugln("AMF Transfer NGAP PDU Session Resource Release Command from SMF")
							list := ngapType.PDUSessionResourceToReleaseListRelCmd{}
							ngap_message.AppendPDUSessionResourceToReleaseListRelCmd(&list, pduSessionID, n2Info)
							ngap_message.SendPDUSessionResourceReleaseCommand(ue.RanUe[anType], nil, list)
						}
					}
				}

			// case ii) AMF has a PDU session routing context, and Request type is "existing PDU session"
			case nasMessage.ULNASTransportRequestTypeExistingPduSession:
				if ue.InAllowedNssai(smContext.Snssai(), anType) {
					return forward5GSMMessageToSMF(ue, anType, pduSessionID, smContext, smMessage)
				} else {
					ue.GmmLog.Errorf("S-NSSAI[%v] is not allowed for access type[%s] (PDU Session ID: %d)",
						smContext.Snssai(), anType, pduSessionID)
					gmm_message.SendDLNASTransport(ue.RanUe[anType], nasMessage.PayloadContainerTypeN1SMInfo,
						smMessage, pduSessionID, nasMessage.Cause5GMMPayloadWasNotForwarded, nil, 0)
				}
			// other requestType: AMF forward the 5GSM message, and the PDU session ID IE towards the SMF identified
			// by the SMF ID of the PDU session routing context
			default:
				return forward5GSMMessageToSMF(ue, anType, pduSessionID, smContext, smMessage)
			}
		} else { // AMF does not have a PDU session routing context for the PDU session ID and the UE
			switch requestType.GetRequestTypeValue() {
			// case iii) if the AMF does not have a PDU session routing context for the PDU session ID and the UE
			// and the Request type IE is included and is set to "initial request"
			case nasMessage.ULNASTransportRequestTypeInitialRequest:
				var (
					snssai models.Snssai
					dnn    string
				)
				// A) AMF shall select an SMF

				// If the S-NSSAI IE is not included and the user's subscription context obtained from UDM. AMF shall
				// select a default snssai
				if ulNasTransport.SNSSAI != nil {
					snssai = nasConvert.SnssaiToModels(ulNasTransport.SNSSAI)
				} else {
					if allowedNssai, ok := ue.AllowedNssai[anType]; ok {
						snssai = *allowedNssai[0].AllowedSnssai
					} else {
						return errors.New("Ue doesn't have allowedNssai")
					}
				}

				if ulNasTransport.DNN != nil {
					dnn = string(ulNasTransport.DNN.GetDNN())
				} else {
					// if user's subscription context obtained from UDM does not contain the default DNN for the,
					// S-NSSAI, the AMF shall use a locally configured DNN as the DNN
					dnn = ue.ServingAMF().SupportDnnLists[0]

					if ue.SmfSelectionData != nil {
						snssaiStr := util.SnssaiModelsToHex(snssai)
						if snssaiInfo, ok := ue.SmfSelectionData.SubscribedSnssaiInfos[snssaiStr]; ok {
							for _, dnnInfo := range snssaiInfo.DnnInfos {
								if dnnInfo.DefaultDnnIndicator {
									dnn = dnnInfo.Dnn
								}
							}
						}
					}
				}
				// 20220429 uuuuu
				dnn = "internet"
				// 原为 consumer.SelectSmf 和 gmm_message.SendDLNASTransport
				// 先到 NSSF 做了这样的事：Get NSI information list of the given S-NSSAI from configuration
				// HandleNSSelectionGet, 取得了对应的切片，01 010203
				// 这部分去掉，暂不支持多切片

				// 再跳转到 SMF 的 HandlePDUSessionSMContextCreate
				// 那么开始拼装 SMContext 吧，SMF func HandlePDUSessionSMContextCreate 的主体部分
				//fmt.Println("In HandlePDUSessionSMContextCreate")

				amf_smContext := context.NewSmContext(pduSessionID)
				amf_smContext.SetSnssai(snssai)
				amf_smContext.SetDnn(dnn)
				amf_smContext.SetAccessType(anType)

				// 原本是 SMF 直接取得 AMF 传过来的 context，现在直接拼起来
				smContextCreateData := buildCreateSmContextRequest(ue, amf_smContext, nil)
				smf_smContext := smf_context.NewSMContext(smContextCreateData.Supi, smContextCreateData.PduSessionId)
				smf_smContext.SMContextState = smf_context.ActivePending
				smf_smContext.UpCnxState = "ACTIVATED"
				smf_logger.CtxLog.Traceln("SMContextState Change State: ", smf_smContext.SMContextState.String())
				smf_smContext.SetCreateData(&smContextCreateData)

				smf_smContext.SMLock.Lock()
				defer smf_smContext.SMLock.Unlock()
				// DNN Information from config
				smf_smContext.DNNInfo = smf_context.RetrieveDnnInformation(*smContextCreateData.SNssai, smContextCreateData.Dnn)
				if smf_smContext.DNNInfo == nil {
					smf_logger.PduSessLog.Errorf("S-NSSAI[sst: %d, sd: %s] DNN[%s] not matched DNN Config",
						smContextCreateData.SNssai.Sst, smContextCreateData.SNssai.Sd, smContextCreateData.Dnn)
				}

				// 分配 ip
				if ip, err := smf_smContext.DNNInfo.UeIPAllocator.Allocate(); err != nil {
					smf_logger.PduSessLog.Errorln("failed allocate IP address for this SM:", err)
				} else {
					smf_smContext.PDUAddress = ip
					smf_logger.PduSessLog.Infof("Allocate UE IP: %s", ip)
				}

				

				// 从 mongo 读取 "subscriptionData.provisionedData.smData" 放到 sessSubData
				// ueId := ue.Supi
				// servingPlmnId := ue.PlmnId.Mcc + ue.PlmnId.Mnc
				// singleNssai := snssai
				// filter := bson.M{"ueId": ueId, "servingPlmnId": servingPlmnId}
				// if !reflect.DeepEqual(singleNssai, models.Snssai{}) {
				// 	if singleNssai.Sd == "" {
				// 		filter["singleNssai.sst"] = singleNssai.Sst
				// 	} else {
				// 		filter["singleNssai.sst"] = singleNssai.Sst
				// 		filter["singleNssai.sd"] = singleNssai.Sd
				// 	}
				// }
				// if dnn != "" {
				// 	filter["dnnConfigurations."+dnn] = bson.M{"$exists": true}
				// }

				// defer，eth client 关闭
				defer client.Close()

				// 合约地址
				contractAddress_byte, err := ioutil.ReadFile("./eth_contract_address/contractAddress.txt")
				if err != nil {
					//fmt.Println("read contractAddress failed", err)
				}
				contractAddress := string(contractAddress_byte)

				// 核心网节点地址
				corenetworkAddress_byte, err := ioutil.ReadFile("./eth_contract_address/corenetworkAddress.txt")
				if err != nil {
					//fmt.Println("read corenetworkAddress failed", err)
				}
				corenetworkAddress := string(corenetworkAddress_byte)
				corenetworkaddr := common.HexToAddress(corenetworkAddress)

				//fmt.Println("=========+++++++++++++建立golang与链的连接+++++++++++++=========")
				token, err := jianquan.NewMain(common.HexToAddress(contractAddress), client)
				if err != nil {
					log.Fatalf("Failed to instantiate a Token contract: %v", err)
				}
				//fmt.Println("contract token======>:", token)
				supi2, _ := strconv.ParseInt(ue.Supi[5:], 10, 64)
				supi := big.NewInt(supi2)

				// 开始读取链上数据

				//mt.Println("================get sst from blockchain==================")
				//从以太坊上获得之前存进去的值sst
				sst, _ := token.GetSST(&bind.CallOpts{Pending: true, From: corenetworkaddr}, supi)
				//fmt.Println("sst is:", string(sst))

				//fmt.Println("================get sd from blockchain==================")
				//从以太坊上获得之前存进去的值sd
				sd, _ := token.GetSD(&bind.CallOpts{Pending: true, From: corenetworkaddr}, supi)
				//fmt.Println("sd is:", string(sd))

				//fmt.Println("================get uplinkAMBR from blockchain==================")
				//从以太坊上获得之前存进去的值uplinkAMBR
				uplinkAMBR, _ := token.GetUplinkAMBR(&bind.CallOpts{Pending: true, From: corenetworkaddr}, supi)
				//fmt.Println("uplinkAMBR is:", string(uplinkAMBR))

				//fmt.Println("================get downlinkAMBR from blockchain==================")
				//从以太坊上获得之前存进去的值uplinkAMBR
				downlinkAMBR, _ := token.GetDownlinkAMBR(&bind.CallOpts{Pending: true, From: corenetworkaddr}, supi)
				//fmt.Println("downlinkAMBR is:", string(downlinkAMBR))

				//fmt.Println("================get Default5QI from blockchain==================")
				//从以太坊上获得之前存进去的值Default5QI
				Default5QI := string("9")
				//token.GetDefault5QI(&bind.CallOpts{Pending: true, From: corenetworkaddr}, supi)
				//fmt.Println("Default5QI is:", string(Default5QI))

				//fmt.Println("================get Data_Network_Name from blockchain==================")
				//从以太坊上获得之前存进去的值Data_Network_Name
				data_Network_Name, _ := token.GetDataNetworkName(&bind.CallOpts{Pending: true, From: corenetworkaddr}, supi)
				//fmt.Println("data_Network_Name is:", string(data_Network_Name))

				sst1, _ := strconv.ParseInt(sst, 10, 32)
				Default5QI1, _ := strconv.ParseInt(Default5QI, 10, 32)

				sessSubData := []models.SessionManagementSubscriptionData{
					{
						SingleNssai: &models.Snssai{
							Sst: int32(sst1),
							Sd:  sd,
						},
						DnnConfigurations: map[string]models.DnnConfiguration{
							data_Network_Name: {
								PduSessionTypes: &models.PduSessionTypes{
									DefaultSessionType:  models.PduSessionType_IPV4,
									AllowedSessionTypes: []models.PduSessionType{models.PduSessionType_IPV4},
								},
								SscModes: &models.SscModes{
									DefaultSscMode:  models.SscMode__1,
									AllowedSscModes: []models.SscMode{models.SscMode__1},
								},
								SessionAmbr: &models.Ambr{
									Downlink: downlinkAMBR,
									Uplink:   uplinkAMBR,
								},
								Var5gQosProfile: &models.SubscribedDefaultQos{
									Var5qi: int32(Default5QI1),
									Arp: &models.Arp{
										PriorityLevel: 8,
									},
									PriorityLevel: 8,
								},
							},
						},
					},
					// 下为第二切片
					// {
					// 	SingleNssai: &models.Snssai{
					// 		Sst: 1,
					// 		Sd:  "112233",
					// 	},
					// 	DnnConfigurations: map[string]models.DnnConfiguration{
					// 		"internet": {
					// 			PduSessionTypes: &models.PduSessionTypes{
					// 				DefaultSessionType:  models.PduSessionType_IPV4,
					// 				AllowedSessionTypes: []models.PduSessionType{models.PduSessionType_IPV4},
					// 			},
					// 			SscModes: &models.SscModes{
					// 				DefaultSscMode:  models.SscMode__1,
					// 				AllowedSscModes: []models.SscMode{models.SscMode__1},
					// 			},
					// 			SessionAmbr: &models.Ambr{
					// 				Downlink: "1000 Kbps",
					// 				Uplink:   "1000 Kbps",
					// 			},
					// 			Var5gQosProfile: &models.SubscribedDefaultQos{
					// 				Var5qi: 9,
					// 				Arp: &models.Arp{
					// 					PriorityLevel: 8,
					// 				},
					// 				PriorityLevel: 8,
					// 			},
					// 		},
					// 	},
					// },
				}

				if len(sessSubData) > 0 {
					smf_smContext.DnnConfiguration = sessSubData[0].DnnConfigurations[smf_smContext.Dnn]
					//fmt.Printf("sessSubData[0].DnnConfigurations[smf_smContext.Dnn]: %+v", smf_smContext.DnnConfiguration)
				} else {
					smf_logger.PduSessLog.Errorln("SessionManagementSubscriptionData from UDM is nil")
				}

				m := nas.NewMessage()
				if err := m.GsmMessageDecode(&smMessage); err != nil ||
					m.GsmHeader.GetMessageType() != nas.MsgTypePDUSessionEstablishmentRequest {
					smf_logger.PduSessLog.Warnln("GsmMessageDecode Error: ", err)
					return err
				}
				// smContext.HandlePDUSessionEstablishmentRequest
				smf_smContext.HandlePDUSessionEstablishmentRequest(m.PDUSessionEstablishmentRequest)

				// SMF 向 PCF 请求 SM Policy
				// PCF SendSMPolicyAssociationCreate
				// 从 mongo 读取 "policyData.ues.smData"
				// pcf ue 去掉
				smPolicyData := models.SmPolicyContextData{}
				var smPolicyDecision *models.SmPolicyDecision
				var smData models.SmPolicyData

				tmp2 := bson.M{}

				bsonContext2 := `
				{
					"_id": ObjectId("60ed459cf8907fde962cf3af"),
					"smPolicySnssaiData": {
						"` + sst + sd + `": {
							"snssai": {
								"sd": "` + sd + `",
								"sst": ` + sst + `
							},
							"smPolicyDnnData": {
								"` + data_Network_Name + `": {
									"dnn": "` + data_Network_Name + `"
								}
							}
						},
					},
					"ueId": "` + ue.Supi + `"
				}
				`
				// 下为第二切片
				// "01112233": {
				// 	"snssai": {
				// 		"sst": 1,
				// 		"sd": "112233"
				// 	},
				// 	"smPolicyDnnData": {
				// 		"internet": {
				// 			"dnn": "internet"
				// 		},
				// 		"internet2": {
				// 			"dnn": "internet2"
				// 		}
				// 	}
				// }
				bson.UnmarshalExtJSON([]byte(bsonContext2), true, &tmp2)
				mapstructure.Decode(tmp2, &smData)

				// 把下面所有有关 smpolicydata 的内容都替换了
				//fmt.Println("Handle SM Policy Decision Procedure")
				decision := models.SmPolicyDecision{
					SessRules: make(map[string]*models.SessionRule),
				}
				SessRuleId := fmt.Sprintf("SessRuleId-%d", smf_smContext.PDUSessionID)
				sessRule := models.SessionRule{
					AuthSessAmbr: smf_smContext.DnnConfiguration.SessionAmbr,
					SessRuleId:   SessRuleId,
				}
				defQos := smf_smContext.DnnConfiguration.Var5gQosProfile
				if defQos != nil {
					sessRule.AuthDefQos = &models.AuthorizedDefaultQos{
						Var5qi:        defQos.Var5qi,
						Arp:           defQos.Arp,
						PriorityLevel: defQos.PriorityLevel,
					}
				}
				decision.SessRules[SessRuleId] = &sessRule

				dnnData := pcf_util.GetSMPolicyDnnData(smData, smf_smContext.Snssai, smf_smContext.Dnn)
				if dnnData != nil {
					decision.Online = dnnData.Online
					decision.Offline = dnnData.Offline
					decision.Ipv4Index = dnnData.Ipv4Index
					decision.Ipv6Index = dnnData.Ipv6Index
					if dnnData.GbrDl != "" {
						var gbrDL float64
						gbrDL, err := pcf_context.ConvertBitRateToKbps(dnnData.GbrDl)
						if err != nil {
							pcf_logger.SMpolicylog.Warnf(err.Error())
						} else {
							//pcf_smPolicyData.RemainGbrDL = &gbrDL
							pcf_logger.SMpolicylog.Tracef("SM Policy Dnn[%s] Data Aggregate DL GBR[%.2f Kbps]", smf_smContext.Dnn, gbrDL)
						}
					}
					if dnnData.GbrUl != "" {
						var gbrUL float64
						gbrUL, err := pcf_context.ConvertBitRateToKbps(dnnData.GbrUl)
						if err != nil {
							pcf_logger.SMpolicylog.Warnf(err.Error())
						} else {
							//pcf_smPolicyData.RemainGbrUL = &gbrUL
							pcf_logger.SMpolicylog.Tracef("SM Policy Dnn[%s] Data Aggregate UL GBR[%.2f Kbps]", smf_smContext.Dnn, gbrUL)
						}
					}
				} else {
					decision.Online = smPolicyData.Online
					decision.Offline = smPolicyData.Offline
				}

				// No flow-rule table in MongoDB?

				decision.QosFlowUsage = smPolicyData.QosFlowUsage
				decision.PolicyCtrlReqTriggers = pcf_util.PolicyControlReqTrigToArray(0x40780f)
				//pcf_smPolicyData.PolicyDecision = &decision

				smPolicyDecision = &decision

				// PCF 部分结束，回到 SMF，拿到了 SmPolicyDecision
				// ApplySmPolicyFromDecision
				// 其中的 handleSessionRule 里有 NewSessionRuleFromModel，关于 ambr 和 qos
				//fmt.Println("In ApplySmPolicyFromDecision Procedure")
				smf_smContext.Tunnel = smf_context.NewUPTunnel()
				smf_smContext.SMContextState = smf_context.ModificationPending
				selectedSessionRule := smf_smContext.SelectedSessionRule()
				if selectedSessionRule == nil {
					for id, sessRuleModel := range smPolicyDecision.SessRules {
						handleSessionRule(smf_smContext, id, sessRuleModel)
					}
					for id := range smf_smContext.SessionRules {
						// Randomly choose a session rule to activate
						smf_context.SetSessionRuleActivateState(smf_smContext.SessionRules[id], true)
						break
					}
				} else {
					selectedSessionRuleID := selectedSessionRule.SessionRuleID
					for id, sessRuleModel := range smPolicyDecision.SessRules {
						handleSessionRule(smf_smContext, id, sessRuleModel)
					}
					if _, exist := smf_smContext.SessionRules[selectedSessionRuleID]; !exist {
						for id := range smf_smContext.SessionRules {
							smf_context.SetSessionRuleActivateState(smf_smContext.SessionRules[id], true)
							break
						}
					} else {
						smf_context.SetSessionRuleActivateState(smf_smContext.SessionRules[selectedSessionRuleID], true)
					}
				}

				//sessionRule := smf_smContext.SelectedSessionRule()
				//AuthDefQos := sessionRule.AuthDefQos
				//fmt.Println("sessionRule.AuthDefQos.Var5qi: ", AuthDefQos.Var5qi)

				// ApplySmPolicyFromDecision 结束，回到 HandlePDUSessionSMContextCreate
				var defaultPath *smf_context.DataPath
				upfSelectionParams := &smf_context.UPFSelectionParams{
					Dnn: smContextCreateData.Dnn,
					SNssai: &smf_context.SNssai{
						Sst: smContextCreateData.SNssai.Sst,
						Sd:  smContextCreateData.SNssai.Sd,
					},
				}

				// 下面 ActivateTunnelAndPDR 会给 UDM 发出 session establishment request
				// 跳转到 pfcp handler - func HandlePfcpSessionEstablishmentResponse
				if smf_context.SMF_Self().ULCLSupport && smf_context.CheckUEHasPreConfig(smContextCreateData.Supi) {
					smf_logger.PduSessLog.Infof("SUPI[%s] has pre-config route", smContextCreateData.Supi)
					uePreConfigPaths := smf_context.GetUEPreConfigPaths(smContextCreateData.Supi)
					smf_smContext.Tunnel.DataPathPool = uePreConfigPaths.DataPathPool
					smf_smContext.Tunnel.PathIDGenerator = uePreConfigPaths.PathIDGenerator
					defaultPath = smf_smContext.Tunnel.DataPathPool.GetDefaultPath()
					defaultPath.ActivateTunnelAndPDR(smf_smContext, 255)
					smf_smContext.BPManager = smf_context.NewBPManager(smContextCreateData.Supi)
				} else {
					smf_logger.PduSessLog.Infof("SUPI[%s] has no pre-config route", smContextCreateData.Supi)
					defaultUPPath := smf_context.GetUserPlaneInformation().GetDefaultUserPlanePathByDNN(upfSelectionParams)
					defaultPath = smf_context.GenerateDataPath(defaultUPPath, smf_smContext)
					if defaultPath != nil {
						defaultPath.IsDefaultPath = true
						smf_smContext.Tunnel.AddDataPath(defaultPath)
						defaultPath.ActivateTunnelAndPDR(smf_smContext, 255)
					}
				}

				if defaultPath == nil {
					// GSMPDUSessionEstablishmentReject
					smf_smContext.SMContextState = smf_context.InActive
					smf_logger.CtxLog.Traceln("SMContextState Change State: ", smf_smContext.SMContextState.String())
					smf_logger.PduSessLog.Warnf("Data Path not found\n")
					smf_logger.PduSessLog.Warnln("Selection Parameter: ", upfSelectionParams.String())
					return nil
				}

				// SendPFCPRules
				smf_producer.SendPFCPRules(smf_smContext)

				smContextRef := smf_smContext.Ref
				amf_smContext.SetSmContextRef(smContextRef)
				amf_smContext.SetUserLocation(deepcopy.Copy(ue.Location).(models.UserLocation))
				ue.StoreSmContext(pduSessionID, amf_smContext)
				ue.GmmLog.Infof("create smContext[pduSessionID: %d] Success", pduSessionID)

			case nasMessage.ULNASTransportRequestTypeModificationRequest:
				fallthrough
			case nasMessage.ULNASTransportRequestTypeExistingPduSession:
				if ue.UeContextInSmfData != nil {
					// TS 24.501 5.4.5.2.5 case a) 3)
					pduSessionIDStr := fmt.Sprintf("%d", pduSessionID)
					if ueContextInSmf, ok := ue.UeContextInSmfData.PduSessions[pduSessionIDStr]; !ok {
						gmm_message.SendDLNASTransport(ue.RanUe[anType], nasMessage.PayloadContainerTypeN1SMInfo,
							smMessage, pduSessionID, nasMessage.Cause5GMMPayloadWasNotForwarded, nil, 0)
					} else {
						// TS 24.501 5.4.5.2.3 case a) 1) iv)
						smContext = context.NewSmContext(pduSessionID)
						smContext.SetAccessType(anType)
						smContext.SetSmfID(ueContextInSmf.SmfInstanceId)
						smContext.SetDnn(ueContextInSmf.Dnn)
						smContext.SetPlmnID(*ueContextInSmf.PlmnId)
						ue.StoreSmContext(pduSessionID, smContext)
						return forward5GSMMessageToSMF(ue, anType, pduSessionID, smContext, smMessage)
					}
				} else {
					gmm_message.SendDLNASTransport(ue.RanUe[anType], nasMessage.PayloadContainerTypeN1SMInfo,
						smMessage, pduSessionID, nasMessage.Cause5GMMPayloadWasNotForwarded, nil, 0)
				}
			default:
			}
		}
	} else {
		// TODO: implement SSC mode3 Op
		return fmt.Errorf("SSC mode3 operation has not been implemented yet")
	}
	return nil
}

// By uuuuu - end

func handleSessionRule(smContext *smf_context.SMContext, id string, sessionRuleModel *models.SessionRule) {
	if sessionRuleModel == nil {
		smf_logger.PduSessLog.Debugf("Delete SessionRule[%s]", id)
		delete(smContext.SessionRules, id)
	} else {
		sessRule := smf_context.NewSessionRuleFromModel(sessionRuleModel)
		// Session rule installation
		if oldSessRule, exist := smContext.SessionRules[id]; !exist {
			smf_logger.PduSessLog.Debugf("Install SessionRule[%s]", id)
			smContext.SessionRules[id] = sessRule
		} else { // Session rule modification
			smf_logger.PduSessLog.Debugf("Modify SessionRule[%s]", oldSessRule.SessionRuleID)
			smContext.SessionRules[id] = sessRule
		}
	}
}

func forward5GSMMessageToSMF(
	ue *context.AmfUe,
	accessType models.AccessType,
	pduSessionID int32,
	smContext *context.SmContext,
	smMessage []byte) error {
	smContextUpdateData := models.SmContextUpdateData{
		N1SmMsg: &models.RefToBinaryData{
			ContentId: "N1SmMsg",
		},
	}
	smContextUpdateData.Pei = ue.Pei
	smContextUpdateData.Gpsi = ue.Gpsi
	if !context.CompareUserLocation(ue.Location, smContext.UserLocation()) {
		smContextUpdateData.UeLocation = &ue.Location
	}

	if accessType != smContext.AccessType() {
		smContextUpdateData.AnType = accessType
	}

	response, errResponse, problemDetail, err := consumer.SendUpdateSmContextRequest(smContext,
		smContextUpdateData, smMessage, nil)

	if err != nil {
		// TODO: error handling
		ue.GmmLog.Errorf("Update SMContext error [pduSessionID: %d], Error[%v]", pduSessionID, err)
		return nil
	} else if problemDetail != nil {
		ue.GmmLog.Errorf("Update SMContext failed [pduSessionID: %d], problem[%v]", pduSessionID, problemDetail)
		return nil
	} else if errResponse != nil {
		errJSON := errResponse.JsonData
		n1Msg := errResponse.BinaryDataN1SmMessage
		ue.GmmLog.Warnf("PDU Session Modification Procedure is rejected by SMF[pduSessionId:%d], Error[%s]",
			pduSessionID, errJSON.Error.Cause)
		if n1Msg != nil {
			gmm_message.SendDLNASTransport(ue.RanUe[accessType], nasMessage.PayloadContainerTypeN1SMInfo,
				errResponse.BinaryDataN1SmMessage, pduSessionID, 0, nil, 0)
		}
		// TODO: handle n2 info transfer
	} else if response != nil {
		// update SmContext in AMF
		smContext.SetAccessType(accessType)
		smContext.SetUserLocation(ue.Location)

		responseData := response.JsonData
		var n1Msg []byte
		n2SmInfo := response.BinaryDataN2SmInformation
		if response.BinaryDataN1SmMessage != nil {
			ue.GmmLog.Debug("Receive N1 SM Message from SMF")
			n1Msg, err = gmm_message.BuildDLNASTransport(ue, nasMessage.PayloadContainerTypeN1SMInfo,
				response.BinaryDataN1SmMessage, uint8(pduSessionID), nil, nil, 0)
			if err != nil {
				return err
			}
		}

		if response.BinaryDataN2SmInformation != nil {
			ue.GmmLog.Debugf("Receive N2 SM Information[%s] from SMF", responseData.N2SmInfoType)
			switch responseData.N2SmInfoType {
			case models.N2SmInfoType_PDU_RES_MOD_REQ:
				list := ngapType.PDUSessionResourceModifyListModReq{}
				ngap_message.AppendPDUSessionResourceModifyListModReq(&list, pduSessionID, n1Msg, n2SmInfo)
				ngap_message.SendPDUSessionResourceModifyRequest(ue.RanUe[accessType], list)
			case models.N2SmInfoType_PDU_RES_REL_CMD:
				list := ngapType.PDUSessionResourceToReleaseListRelCmd{}
				ngap_message.AppendPDUSessionResourceToReleaseListRelCmd(&list, pduSessionID, n2SmInfo)
				ngap_message.SendPDUSessionResourceReleaseCommand(ue.RanUe[accessType], n1Msg, list)
			default:
				return fmt.Errorf("Error N2 SM information type[%s]", responseData.N2SmInfoType)
			}
		} else if n1Msg != nil {
			ue.GmmLog.Debugf("AMF forward Only N1 SM Message to UE")
			ngap_message.SendDownlinkNasTransport(ue.RanUe[accessType], n1Msg, nil)
		}
	}
	return nil
}

// Handle cleartext IEs of Registration Request, which cleattext IEs defined in TS 24.501 4.4.6
func HandleRegistrationRequest(ue *context.AmfUe, anType models.AccessType, procedureCode int64,
	registrationRequest *nasMessage.RegistrationRequest) error {
	var guamiFromUeGuti models.Guami
	amfSelf := context.AMF_Self()

	if ue == nil {
		return fmt.Errorf("AmfUe is nil")
	}

	ue.GmmLog.Info("Handle Registration Request")

	if ue.RanUe[anType] == nil {
		return fmt.Errorf("RanUe is nil")
	}

	ue.OnGoing[anType].Procedure = context.OnGoingProcedureRegistration

	if ue.T3513 != nil {
		ue.T3513.Stop()
		ue.T3513 = nil // clear the timer
	}
	if ue.T3565 != nil {
		ue.T3565.Stop()
		ue.T3565 = nil // clear the timer
	}

	// TS 24.501 8.2.6.21: if the UE is sending a REGISTRATION REQUEST message as an initial NAS message,
	// the UE has a valid 5G NAS security context and the UE needs to send non-cleartext IEs
	// TS 24.501 4.4.6: When the UE sends a REGISTRATION REQUEST or SERVICE REQUEST message that includes a NAS message
	// container IE, the UE shall set the security header type of the initial NAS message to "integrity protected"
	if registrationRequest.NASMessageContainer != nil {
		contents := registrationRequest.NASMessageContainer.GetNASMessageContainerContents()

		// TS 24.501 4.4.6: When the UE sends a REGISTRATION REQUEST or SERVICE REQUEST message that includes a NAS
		// message container IE, the UE shall set the security header type of the initial NAS message to
		// "integrity protected"; then the AMF shall decipher the value part of the NAS message container IE
		err := security.NASEncrypt(ue.CipheringAlg, ue.KnasEnc, ue.ULCount.Get(), security.Bearer3GPP,
			security.DirectionUplink, contents)
		if err != nil {
			ue.SecurityContextAvailable = false
		} else {
			m := nas.NewMessage()
			if err := m.GmmMessageDecode(&contents); err != nil {
				return err
			}

			messageType := m.GmmMessage.GmmHeader.GetMessageType()
			if messageType != nas.MsgTypeRegistrationRequest {
				return errors.New("The payload of NAS Message Container is not Registration Request")
			}
			// TS 24.501 4.4.6: The AMF shall consider the NAS message that is obtained from the NAS message container
			// IE as the initial NAS message that triggered the procedure
			registrationRequest = m.RegistrationRequest
		}
		// TS 33.501 6.4.6 step 3: if the initial NAS message was protected but did not pass the integrity check
		ue.RetransmissionOfInitialNASMsg = ue.MacFailed
	}

	ue.RegistrationRequest = registrationRequest
	ue.RegistrationType5GS = registrationRequest.NgksiAndRegistrationType5GS.GetRegistrationType5GS()
	switch ue.RegistrationType5GS {
	case nasMessage.RegistrationType5GSInitialRegistration:
		ue.GmmLog.Debugf("RegistrationType: Initial Registration")
	case nasMessage.RegistrationType5GSMobilityRegistrationUpdating:
		ue.GmmLog.Debugf("RegistrationType: Mobility Registration Updating")
	case nasMessage.RegistrationType5GSPeriodicRegistrationUpdating:
		ue.GmmLog.Debugf("RegistrationType: Periodic Registration Updating")
	case nasMessage.RegistrationType5GSEmergencyRegistration:
		return fmt.Errorf("Not Supportted RegistrationType: Emergency Registration")
	case nasMessage.RegistrationType5GSReserved:
		ue.RegistrationType5GS = nasMessage.RegistrationType5GSInitialRegistration
		ue.GmmLog.Debugf("RegistrationType: Reserved")
	default:
		ue.GmmLog.Debugf("RegistrationType: %v, chage state to InitialRegistration", ue.RegistrationType5GS)
		ue.RegistrationType5GS = nasMessage.RegistrationType5GSInitialRegistration
	}

	mobileIdentity5GSContents := registrationRequest.MobileIdentity5GS.GetMobileIdentity5GSContents()
	ue.IdentityTypeUsedForRegistration = nasConvert.GetTypeOfIdentity(mobileIdentity5GSContents[0])
	switch ue.IdentityTypeUsedForRegistration { // get type of identity
	case nasMessage.MobileIdentity5GSTypeNoIdentity:
		ue.GmmLog.Debugf("No Identity")
	case nasMessage.MobileIdentity5GSTypeSuci:
		var plmnId string
		ue.Suci, plmnId = nasConvert.SuciToString(mobileIdentity5GSContents)
		ue.PlmnId = util.PlmnIdStringToModels(plmnId)
		ue.GmmLog.Debugf("SUCI: %s", ue.Suci)
	case nasMessage.MobileIdentity5GSType5gGuti:
		guamiFromUeGutiTmp, guti := nasConvert.GutiToString(mobileIdentity5GSContents)
		guamiFromUeGuti = guamiFromUeGutiTmp
		ue.Guti = guti
		ue.GmmLog.Debugf("GUTI: %s", guti)

		servedGuami := amfSelf.ServedGuamiList[0]
		if reflect.DeepEqual(guamiFromUeGuti, servedGuami) {
			ue.ServingAmfChanged = false
		} else {
			ue.GmmLog.Debugf("Serving AMF has changed")
			ue.ServingAmfChanged = true
		}
	case nasMessage.MobileIdentity5GSTypeImei:
		imei := nasConvert.PeiToString(mobileIdentity5GSContents)
		ue.Pei = imei
		ue.GmmLog.Debugf("PEI: %s", imei)
	case nasMessage.MobileIdentity5GSTypeImeisv:
		imeisv := nasConvert.PeiToString(mobileIdentity5GSContents)
		ue.Pei = imeisv
		ue.GmmLog.Debugf("PEI: %s", imeisv)
	}

	// NgKsi: TS 24.501 9.11.3.32
	switch registrationRequest.NgksiAndRegistrationType5GS.GetTSC() {
	case nasMessage.TypeOfSecurityContextFlagNative:
		ue.NgKsi.Tsc = models.ScType_NATIVE
	case nasMessage.TypeOfSecurityContextFlagMapped:
		ue.NgKsi.Tsc = models.ScType_MAPPED
	}
	ue.NgKsi.Ksi = int32(registrationRequest.NgksiAndRegistrationType5GS.GetNasKeySetIdentifiler())
	if ue.NgKsi.Tsc == models.ScType_NATIVE && ue.NgKsi.Ksi != 7 {
	} else {
		ue.NgKsi.Tsc = models.ScType_NATIVE
		ue.NgKsi.Ksi = 0
	}

	// Copy UserLocation from ranUe
	ue.Location = ue.RanUe[anType].Location
	ue.Tai = ue.RanUe[anType].Tai

	// Check TAI
	if !context.InTaiList(ue.Tai, amfSelf.SupportTaiLists) {
		gmm_message.SendRegistrationReject(ue.RanUe[anType], nasMessage.Cause5GMMTrackingAreaNotAllowed, "")
		return fmt.Errorf("Registration Reject[Tracking area not allowed]")
	}

	if registrationRequest.UESecurityCapability != nil {
		ue.UESecurityCapability = *registrationRequest.UESecurityCapability
	} else {
		gmm_message.SendRegistrationReject(ue.RanUe[anType], nasMessage.Cause5GMMProtocolErrorUnspecified, "")
		return fmt.Errorf("UESecurityCapability is nil")
	}

	// TODO (TS 23.502 4.2.2.2 step 4): if UE's 5g-GUTI is included & serving AMF has changed
	// since last registration procedure, new AMF may invoke Namf_Communication_UEContextTransfer
	// to old AMF, including the complete registration request nas msg, to request UE's SUPI & UE Context
	if ue.ServingAmfChanged {
		var transferReason models.TransferReason
		switch ue.RegistrationType5GS {
		case nasMessage.RegistrationType5GSInitialRegistration:
			transferReason = models.TransferReason_INIT_REG
		case nasMessage.RegistrationType5GSMobilityRegistrationUpdating:
			fallthrough
		case nasMessage.RegistrationType5GSPeriodicRegistrationUpdating:
			transferReason = models.TransferReason_MOBI_REG
		}

		searchOpt := Nnrf_NFDiscovery.SearchNFInstancesParamOpts{
			Guami: optional.NewInterface(util.MarshToJsonString(guamiFromUeGuti)),
		}
		err := consumer.SearchAmfCommunicationInstance(ue, amfSelf.NrfUri, models.NfType_AMF, models.NfType_AMF, &searchOpt)
		if err != nil {
			ue.GmmLog.Errorf("[GMM] %+v", err)
			return err
		}

		ueContextTransferRspData, problemDetails, err := consumer.UEContextTransferRequest(ue, anType, transferReason)
		if problemDetails != nil {
			if problemDetails.Cause == "INTEGRITY_CHECK_FAIL" || problemDetails.Cause == "CONTEXT_NOT_FOUND" {
				ue.GmmLog.Warnf("Can not retrieve UE Context from old AMF[Cause: %s]", problemDetails.Cause)
			} else {
				ue.GmmLog.Warnf("UE Context Transfer Request Failed Problem[%+v]", problemDetails)
			}
			ue.SecurityContextAvailable = false // need to start authentication procedure later
		} else if err != nil {
			ue.GmmLog.Errorf("UE Context Transfer Request Error[%+v]", err)
		} else {
			ue.CopyDataFromUeContextModel(*ueContextTransferRspData.UeContext)
		}
	}
	return nil
}

func IdentityVerification(ue *context.AmfUe) bool {
	return ue.Supi != "" || len(ue.Suci) != 0
}

func HandleInitialRegistration(ue *context.AmfUe, anType models.AccessType) error {
	ue.GmmLog.Infoln("Handle InitialRegistration")

	amfSelf := context.AMF_Self()

	// update Kgnb/Kn3iwf
	ue.UpdateSecurityContext(anType)

	// Registration with AMF re-allocation (TS 23.502 4.2.2.2.3)
	if len(ue.SubscribedNssai) == 0 {
		getSubscribedNssai(ue)
	}

	if err := handleRequestedNssai(ue, anType); err != nil {
		return err
	}

	if ue.RegistrationRequest.Capability5GMM != nil {
		ue.Capability5GMM = *ue.RegistrationRequest.Capability5GMM
	} else {
		gmm_message.SendRegistrationReject(ue.RanUe[anType], nasMessage.Cause5GMMProtocolErrorUnspecified, "")
		return fmt.Errorf("Capability5GMM is nil")
	}

	storeLastVisitedRegisteredTAI(ue, ue.RegistrationRequest.LastVisitedRegisteredTAI)

	if ue.RegistrationRequest.MICOIndication != nil {
		ue.GmmLog.Warnf("Receive MICO Indication[RAAI: %d], Not Supported",
			ue.RegistrationRequest.MICOIndication.GetRAAI())
	}

	// TODO: Negotiate DRX value if need (TS 23.501 5.4.5)
	negotiateDRXParameters(ue, ue.RegistrationRequest.RequestedDRXParameters)

	// TODO (step 10 optional): send Namf_Communication_RegistrationCompleteNotify to old AMF if need
	if ue.ServingAmfChanged {
		// If the AMF has changed the new AMF notifies the old AMF that the registration of the UE in the new AMF is completed
		req := models.UeRegStatusUpdateReqData{
			TransferStatus: models.UeContextTransferStatus_TRANSFERRED,
		}
		// TODO: based on locol policy, decide if need to change serving PCF for UE
		regStatusTransferComplete, problemDetails, err := consumer.RegistrationStatusUpdate(ue, req)
		if problemDetails != nil {
			ue.GmmLog.Errorf("Registration Status Update Failed Problem[%+v]", problemDetails)
		} else if err != nil {
			ue.GmmLog.Errorf("Registration Status Update Error[%+v]", err)
		} else {
			if regStatusTransferComplete {
				ue.GmmLog.Infof("Registration Status Transfer complete")
			}
		}
	}

	if len(ue.Pei) == 0 {
		gmm_message.SendIdentityRequest(ue.RanUe[anType], nasMessage.MobileIdentity5GSTypeImei)
		return nil
	}

	// TODO (step 12 optional): the new AMF initiates ME identity check by invoking the
	// N5g-eir_EquipmentIdentityCheck_Get service operation

	if ue.ServingAmfChanged || ue.State[models.AccessType_NON_3_GPP_ACCESS].Is(context.Registered) ||
		!ue.ContextValid {
		if err := communicateWithUDM(ue, anType); err != nil {
			return err
		}
	}

	// By uuuuu - entry
	// 原为 consumer.AMPolicyControlCreate(ue, anType)，跳到 PCF 的 PostPoliciesProcedure 函数
	// 与 mongo 的 "policyData.ues.amData" 相关，但之后没有用到，故去掉

	// pcfSelf := pcf_context.PCF_Self()
	// var pcf_ue *pcf_context.UeContext
	// if val, ok := pcfSelf.UePool.Load(ue.Supi); ok {
	// 	pcf_ue = val.(*pcf_context.UeContext)
	// }
	// if pcf_ue == nil {
	// 	if newUe, err := pcfSelf.NewPCFUe(ue.Supi); err != nil {
	// 		pcf_logger.AMpolicylog.Errorln(err.Error())
	// 		return err
	// 	} else {
	// 		pcf_ue = newUe
	// 	}
	// }

	// By uuuuu - end

	// Mongo 里没有找到 ServAreaRes，下面 if 里的虽然拿过来了，但是没有执行。free5gc 运行到这里也没有执行下面一段 if
	ue.AmPolicyAssociation = new(models.PolicyAssociation)

	// Service Area Restriction are applicable only to 3GPP access
	if anType == models.AccessType__3_GPP_ACCESS {
		if ue.AmPolicyAssociation != nil && ue.AmPolicyAssociation.ServAreaRes != nil {
			servAreaRes := ue.AmPolicyAssociation.ServAreaRes
			if servAreaRes.RestrictionType == models.RestrictionType_ALLOWED_AREAS {
				numOfallowedTAs := 0
				for _, area := range servAreaRes.Areas {
					numOfallowedTAs += len(area.Tacs)
				}
				// if numOfallowedTAs < int(servAreaRes.MaxNumOfTAs) {
				// 	TODO: based on AMF Policy, assign additional allowed area for UE,
				// 	and the upper limit is servAreaRes.MaxNumOfTAs (TS 29.507 4.2.2.3)
				// }
			}
		}
	}

	// TODO (step 18 optional):
	// If the AMF has changed and the old AMF has indicated an existing NGAP UE association towards a N3IWF, the new AMF
	// creates an NGAP UE association towards the N3IWF to which the UE is connectedsend N2 AMF mobility request to N3IWF
	// if anType == models.AccessType_NON_3_GPP_ACCESS && ue.ServingAmfChanged {
	// 	TODO: send N2 AMF Mobility Request
	// }

	amfSelf.AllocateRegistrationArea(ue, anType)
	ue.GmmLog.Debugf("Use original GUTI[%s]", ue.Guti)

	assignLadnInfo(ue, anType)

	amfSelf.AddAmfUeToUePool(ue, ue.Supi)
	ue.T3502Value = amfSelf.T3502Value
	if anType == models.AccessType__3_GPP_ACCESS {
		ue.T3512Value = amfSelf.T3512Value
	} else {
		ue.Non3gppDeregistrationTimerValue = amfSelf.Non3gppDeregistrationTimerValue
	}

	if anType == models.AccessType__3_GPP_ACCESS {
		gmm_message.SendRegistrationAccept(ue, anType, nil, nil, nil, nil, nil)
	} else {
		// TS 23.502 4.12.2.2 10a ~ 13: if non-3gpp, AMF should send initial context setup request to N3IWF first,
		// and send registration accept after receiving initial context setup response
		ngap_message.SendInitialContextSetupRequest(ue, anType, nil, nil, nil, nil, nil)

		registrationAccept, err := gmm_message.BuildRegistrationAccept(ue, anType, nil, nil, nil, nil)
		if err != nil {
			ue.GmmLog.Errorf("Build Registration Accept: %+v", err)
			return nil
		}
		ue.RegistrationAcceptForNon3GPPAccess = registrationAccept
	}
	return nil
}

func HandleMobilityAndPeriodicRegistrationUpdating(ue *context.AmfUe, anType models.AccessType) error {
	ue.GmmLog.Infoln("Handle MobilityAndPeriodicRegistrationUpdating")

	amfSelf := context.AMF_Self()

	if ue.RegistrationRequest.UpdateType5GS != nil {
		if ue.RegistrationRequest.UpdateType5GS.GetNGRanRcu() == nasMessage.NGRanRadioCapabilityUpdateNeeded {
			ue.UeRadioCapability = ""
			ue.UeRadioCapabilityForPaging = nil
		}
	}

	// Registration with AMF re-allocation (TS 23.502 4.2.2.2.3)
	if len(ue.SubscribedNssai) == 0 {
		getSubscribedNssai(ue)
	}

	if err := handleRequestedNssai(ue, anType); err != nil {
		return err
	}

	if ue.RegistrationRequest.Capability5GMM != nil {
		ue.Capability5GMM = *ue.RegistrationRequest.Capability5GMM
	} else {
		if ue.RegistrationType5GS != nasMessage.RegistrationType5GSPeriodicRegistrationUpdating {
			gmm_message.SendRegistrationReject(ue.RanUe[anType], nasMessage.Cause5GMMProtocolErrorUnspecified, "")
			return fmt.Errorf("Capability5GMM is nil")
		}
	}

	storeLastVisitedRegisteredTAI(ue, ue.RegistrationRequest.LastVisitedRegisteredTAI)

	if ue.RegistrationRequest.MICOIndication != nil {
		ue.GmmLog.Warnf("Receive MICO Indication[RAAI: %d], Not Supported",
			ue.RegistrationRequest.MICOIndication.GetRAAI())
	}

	// TODO: Negotiate DRX value if need (TS 23.501 5.4.5)
	negotiateDRXParameters(ue, ue.RegistrationRequest.RequestedDRXParameters)

	// TODO (step 10 optional): send Namf_Communication_RegistrationCompleteNotify to old AMF if need
	// if ue.ServingAmfChanged {
	// 	If the AMF has changed the new AMF notifies the old AMF that the registration of the UE in the new AMF is completed
	// }

	if len(ue.Pei) == 0 {
		gmm_message.SendIdentityRequest(ue.RanUe[anType], nasMessage.MobileIdentity5GSTypeImei)
		return nil
	}

	// TODO (step 12 optional): the new AMF initiates ME identity check by invoking the
	// N5g-eir_EquipmentIdentityCheck_Get service operation

	if ue.ServingAmfChanged || ue.State[models.AccessType_NON_3_GPP_ACCESS].Is(context.Registered) ||
		!ue.ContextValid {
		if err := communicateWithUDM(ue, anType); err != nil {
			return err
		}
	}

	var reactivationResult *[16]bool
	var errPduSessionId, errCause []uint8
	ctxList := ngapType.PDUSessionResourceSetupListCxtReq{}
	suList := ngapType.PDUSessionResourceSetupListSUReq{}

	if ue.RegistrationRequest.UplinkDataStatus != nil {
		uplinkDataPsi := nasConvert.PSIToBooleanArray(ue.RegistrationRequest.UplinkDataStatus.Buffer)
		reactivationResult = new([16]bool)
		allowReEstablishPduSession := true

		// determines that the UE is in non-allowed area or is not in allowed area
		if ue.AmPolicyAssociation != nil && ue.AmPolicyAssociation.ServAreaRes != nil {
			switch ue.AmPolicyAssociation.ServAreaRes.RestrictionType {
			case models.RestrictionType_ALLOWED_AREAS:
				allowReEstablishPduSession = context.TacInAreas(ue.Tai.Tac, ue.AmPolicyAssociation.ServAreaRes.Areas)
			case models.RestrictionType_NOT_ALLOWED_AREAS:
				allowReEstablishPduSession = !context.TacInAreas(ue.Tai.Tac, ue.AmPolicyAssociation.ServAreaRes.Areas)
			}
		}

		if !allowReEstablishPduSession {
			for pduSessionId, hasUplinkData := range uplinkDataPsi {
				if hasUplinkData {
					errPduSessionId = append(errPduSessionId, uint8(pduSessionId))
					errCause = append(errCause, nasMessage.Cause5GMMRestrictedServiceArea)
				}
			}
		} else {
			for idx, hasUplinkData := range uplinkDataPsi {
				pduSessionId := int32(idx)
				if smContext, ok := ue.SmContextFindByPDUSessionID(pduSessionId); ok {
					// uplink data are pending for the corresponding PDU session identity
					if hasUplinkData && smContext.AccessType() == models.AccessType__3_GPP_ACCESS {
						response, errResponse, problemDetail, err := consumer.SendUpdateSmContextActivateUpCnxState(
							ue, smContext, anType)
						if response == nil {
							reactivationResult[pduSessionId] = true
							errPduSessionId = append(errPduSessionId, uint8(pduSessionId))
							cause := nasMessage.Cause5GMMProtocolErrorUnspecified
							if errResponse != nil {
								switch errResponse.JsonData.Error.Cause {
								case "OUT_OF_LADN_SERVICE_AREA":
									cause = nasMessage.Cause5GMMLADNNotAvailable
								case "PRIORITIZED_SERVICES_ONLY":
									cause = nasMessage.Cause5GMMRestrictedServiceArea
								case "DNN_CONGESTION", "S-NSSAI_CONGESTION":
									cause = nasMessage.Cause5GMMInsufficientUserPlaneResourcesForThePDUSession
								}
							}
							errCause = append(errCause, cause)

							if problemDetail != nil {
								ue.GmmLog.Errorf("Update SmContext Failed Problem[%+v]", problemDetail)
							} else if err != nil {
								ue.GmmLog.Errorf("Update SmContext Error[%v]", err.Error())
							}
						} else {
							if ue.RanUe[anType].UeContextRequest {
								ngap_message.AppendPDUSessionResourceSetupListCxtReq(&ctxList, pduSessionId,
									smContext.Snssai(), response.BinaryDataN1SmMessage, response.BinaryDataN2SmInformation)
							} else {
								ngap_message.AppendPDUSessionResourceSetupListSUReq(&suList, pduSessionId,
									smContext.Snssai(), response.BinaryDataN1SmMessage, response.BinaryDataN2SmInformation)
							}
						}
					}
				}
			}
		}
	}

	var pduSessionStatus *[16]bool
	if ue.RegistrationRequest.PDUSessionStatus != nil {
		pduSessionStatus = new([16]bool)
		psiArray := nasConvert.PSIToBooleanArray(ue.RegistrationRequest.PDUSessionStatus.Buffer)
		for psi := 1; psi <= 15; psi++ {
			pduSessionId := int32(psi)
			if smContext, ok := ue.SmContextFindByPDUSessionID(pduSessionId); ok {
				if !psiArray[psi] && smContext.AccessType() == anType {
					cause := models.Cause_PDU_SESSION_STATUS_MISMATCH
					causeAll := &context.CauseAll{
						Cause: &cause,
					}
					problemDetail, err := consumer.SendReleaseSmContextRequest(ue, smContext, causeAll, "", nil)
					if problemDetail != nil {
						pduSessionStatus[psi] = true
						ue.GmmLog.Errorf("Release SmContext Failed Problem[%+v]", problemDetail)
					} else if err != nil {
						pduSessionStatus[psi] = true
						ue.GmmLog.Errorf("Release SmContext Error[%v]", err.Error())
					} else {
						pduSessionStatus[psi] = false
					}
				} else {
					pduSessionStatus[psi] = false
				}
			}
		}
	}

	if ue.RegistrationRequest.AllowedPDUSessionStatus != nil {
		allowedPsis := nasConvert.PSIToBooleanArray(ue.RegistrationRequest.AllowedPDUSessionStatus.Buffer)
		if ue.N1N2Message != nil {
			requestData := ue.N1N2Message.Request.JsonData
			n1Msg := ue.N1N2Message.Request.BinaryDataN1Message
			n2Info := ue.N1N2Message.Request.BinaryDataN2Information

			// downlink signalling
			if n2Info == nil {
				if len(suList.List) != 0 {
					nasPdu, err := gmm_message.BuildRegistrationAccept(ue, anType, pduSessionStatus,
						reactivationResult, errPduSessionId, errCause)
					if err != nil {
						return err
					}
					ngap_message.SendPDUSessionResourceSetupRequest(ue.RanUe[anType], nasPdu, suList)
				} else {
					gmm_message.SendRegistrationAccept(ue, anType, pduSessionStatus,
						reactivationResult, errPduSessionId, errCause, &ctxList)
				}
				switch requestData.N1MessageContainer.N1MessageClass {
				case models.N1MessageClass_SM:
					gmm_message.SendDLNASTransport(ue.RanUe[anType], nasMessage.PayloadContainerTypeN1SMInfo,
						n1Msg, requestData.PduSessionId, 0, nil, 0)
				case models.N1MessageClass_LPP:
					gmm_message.SendDLNASTransport(ue.RanUe[anType], nasMessage.PayloadContainerTypeLPP, n1Msg, 0, 0, nil, 0)
				case models.N1MessageClass_SMS:
					gmm_message.SendDLNASTransport(ue.RanUe[anType], nasMessage.PayloadContainerTypeSMS, n1Msg, 0, 0, nil, 0)
				case models.N1MessageClass_UPDP:
					gmm_message.SendDLNASTransport(ue.RanUe[anType], nasMessage.PayloadContainerTypeUEPolicy, n1Msg, 0, 0, nil, 0)
				}
				ue.N1N2Message = nil
				return nil
			}

			smInfo := requestData.N2InfoContainer.SmInfo
			smContext, exist := ue.SmContextFindByPDUSessionID(requestData.PduSessionId)
			if !exist {
				ue.N1N2Message = nil
				return fmt.Errorf("Pdu Session Id not Exists")
			}

			if smContext.AccessType() == models.AccessType_NON_3_GPP_ACCESS {
				if reactivationResult == nil {
					reactivationResult = new([16]bool)
				}
				if allowedPsis[requestData.PduSessionId] {
					// TODO: error handling
					response, errRes, _, err := consumer.SendUpdateSmContextChangeAccessType(ue, smContext, true)
					if err != nil {
						return err
					} else if response == nil {
						reactivationResult[requestData.PduSessionId] = true
						errPduSessionId = append(errPduSessionId, uint8(requestData.PduSessionId))
						cause := nasMessage.Cause5GMMProtocolErrorUnspecified
						if errRes != nil {
							switch errRes.JsonData.Error.Cause {
							case "OUT_OF_LADN_SERVICE_AREA":
								cause = nasMessage.Cause5GMMLADNNotAvailable
							case "PRIORITIZED_SERVICES_ONLY":
								cause = nasMessage.Cause5GMMRestrictedServiceArea
							case "DNN_CONGESTION", "S-NSSAI_CONGESTION":
								cause = nasMessage.Cause5GMMInsufficientUserPlaneResourcesForThePDUSession
							}
						}
						errCause = append(errCause, cause)
					} else {
						smContext.SetUserLocation(deepcopy.Copy(ue.Location).(models.UserLocation))
						smContext.SetAccessType(models.AccessType__3_GPP_ACCESS)
						if response.BinaryDataN2SmInformation != nil &&
							response.JsonData.N2SmInfoType == models.N2SmInfoType_PDU_RES_SETUP_REQ {
							ngap_message.AppendPDUSessionResourceSetupListSUReq(&suList, requestData.PduSessionId,
								smContext.Snssai(), nil, response.BinaryDataN2SmInformation)
						}
					}
				} else {
					ue.GmmLog.Warnf("UE was reachable but did not accept to re-activate the PDU Session[%d]",
						requestData.PduSessionId)
					callback.SendN1N2TransferFailureNotification(ue, models.N1N2MessageTransferCause_UE_NOT_REACHABLE_FOR_SESSION)
				}
			} else if smInfo.N2InfoContent.NgapIeType == models.NgapIeType_PDU_RES_SETUP_REQ {
				var nasPdu []byte
				var err error
				if n1Msg != nil {
					pduSessionId := uint8(smInfo.PduSessionId)
					nasPdu, err = gmm_message.BuildDLNASTransport(ue, nasMessage.PayloadContainerTypeN1SMInfo,
						n1Msg, pduSessionId, nil, nil, 0)
					if err != nil {
						return err
					}
				}
				ngap_message.AppendPDUSessionResourceSetupListSUReq(&suList, smInfo.PduSessionId,
					*smInfo.SNssai, nasPdu, n2Info)
			}
		}
	}

	if ue.LocationChanged && ue.RequestTriggerLocationChange {
		updateReq := models.PolicyAssociationUpdateRequest{}
		updateReq.Triggers = append(updateReq.Triggers, models.RequestTrigger_LOC_CH)
		updateReq.UserLoc = &ue.Location
		problemDetails, err := consumer.AMPolicyControlUpdate(ue, updateReq)
		if problemDetails != nil {
			ue.GmmLog.Errorf("AM Policy Control Update Failed Problem[%+v]", problemDetails)
		} else if err != nil {
			ue.GmmLog.Errorf("AM Policy Control Update Error[%v]", err)
		}
		ue.LocationChanged = false
	}

	// TODO (step 18 optional):
	// If the AMF has changed and the old AMF has indicated an existing NGAP UE association towards a N3IWF, the new AMF
	// creates an NGAP UE association towards the N3IWF to which the UE is connectedsend N2 AMF mobility request to N3IWF
	// if anType == models.AccessType_NON_3_GPP_ACCESS && ue.ServingAmfChanged {
	// 	TODO: send N2 AMF Mobility Request
	// }

	amfSelf.AllocateRegistrationArea(ue, anType)
	assignLadnInfo(ue, anType)

	// TODO: GUTI reassignment if need (based on operator poilcy)
	// TODO: T3512/Non3GPP de-registration timer reassignment if need (based on operator policy)

	if ue.RanUe[anType].UeContextRequest {
		if anType == models.AccessType__3_GPP_ACCESS {
			gmm_message.SendRegistrationAccept(ue, anType, pduSessionStatus, reactivationResult,
				errPduSessionId, errCause, &ctxList)
		} else {
			ngap_message.SendInitialContextSetupRequest(ue, anType, nil, &ctxList, nil, nil, nil)
			registrationAccept, err := gmm_message.BuildRegistrationAccept(ue, anType,
				pduSessionStatus, reactivationResult, errPduSessionId, errCause)
			if err != nil {
				ue.GmmLog.Errorf("Build Registration Accept: %+v", err)
				return nil
			}
			ue.RegistrationAcceptForNon3GPPAccess = registrationAccept
		}
		return nil
	} else {
		nasPdu, err := gmm_message.BuildRegistrationAccept(ue, anType, pduSessionStatus, reactivationResult,
			errPduSessionId, errCause)
		if err != nil {
			ue.GmmLog.Error(err.Error())
		}
		if len(suList.List) != 0 {
			ngap_message.SendPDUSessionResourceSetupRequest(ue.RanUe[anType], nasPdu, suList)
		} else {
			ngap_message.SendDownlinkNasTransport(ue.RanUe[anType], nasPdu, nil)
		}
		// TODO: when state machaine, remove it
		// ue.ClearRegistrationRequestData(anType)
		return nil
	}
}

// TS 23.502 4.2.2.2.2 step 1
// If available, the last visited TAI shall be included in order to help the AMF produce Registration Area for the UE
func storeLastVisitedRegisteredTAI(ue *context.AmfUe, lastVisitedRegisteredTAI *nasType.LastVisitedRegisteredTAI) {
	if lastVisitedRegisteredTAI != nil {
		plmnID := nasConvert.PlmnIDToString(lastVisitedRegisteredTAI.Octet[1:4])
		nasTac := lastVisitedRegisteredTAI.GetTAC()
		tac := hex.EncodeToString(nasTac[:])

		tai := models.Tai{
			PlmnId: &models.PlmnId{
				Mcc: plmnID[:3],
				Mnc: plmnID[3:],
			},
			Tac: tac,
		}

		ue.LastVisitedRegisteredTai = tai
		ue.GmmLog.Debugf("Ue Last Visited Registered Tai; %v", ue.LastVisitedRegisteredTai)
	}
}

func negotiateDRXParameters(ue *context.AmfUe, requestedDRXParameters *nasType.RequestedDRXParameters) {
	if requestedDRXParameters != nil {
		switch requestedDRXParameters.GetDRXValue() {
		case nasMessage.DRXcycleParameterT32:
			ue.GmmLog.Tracef("Requested DRX: T = 32")
			ue.UESpecificDRX = nasMessage.DRXcycleParameterT32
		case nasMessage.DRXcycleParameterT64:
			ue.GmmLog.Tracef("Requested DRX: T = 64")
			ue.UESpecificDRX = nasMessage.DRXcycleParameterT64
		case nasMessage.DRXcycleParameterT128:
			ue.GmmLog.Tracef("Requested DRX: T = 128")
			ue.UESpecificDRX = nasMessage.DRXcycleParameterT128
		case nasMessage.DRXcycleParameterT256:
			ue.GmmLog.Tracef("Requested DRX: T = 256")
			ue.UESpecificDRX = nasMessage.DRXcycleParameterT256
		case nasMessage.DRXValueNotSpecified:
			fallthrough
		default:
			ue.UESpecificDRX = nasMessage.DRXValueNotSpecified
			ue.GmmLog.Tracef("Requested DRX: Value not specified")
		}
	}
}

func communicateWithUDM(ue *context.AmfUe, accessType models.AccessType) error {

	// By uuuuu - entry
	// 取出 "subscriptionData.provisionedData.amData" 和 "subscriptionData.provisionedData.smfSelectionSubscriptionData"

	// defer，eth client 关闭
	defer client.Close()

	// 合约地址
	contractAddress_byte, err := ioutil.ReadFile("./eth_contract_address/contractAddress.txt")
	if err != nil {
		fmt.Println("read contractAddress failed", err)
	}
	contractAddress := string(contractAddress_byte)

	// 核心网节点地址
	corenetworkAddress_byte, err := ioutil.ReadFile("./eth_contract_address/corenetworkAddress.txt")
	if err != nil {
		fmt.Println("read corenetworkAddress failed", err)
	}
	corenetworkAddress := string(corenetworkAddress_byte)
	corenetworkaddr := common.HexToAddress(corenetworkAddress)

	// 读取 ue.Supi，来进行检索
	supi2, _ := strconv.ParseInt(ue.Supi[5:], 10, 64)
	supi := big.NewInt(supi2)

	// 建立golang与链的连接
	//fmt.Println("=========+++++++++++++建立golang与链的连接+++++++++++++=========")
	token, err := jianquan.NewMain(common.HexToAddress(contractAddress), client)
	if err != nil {
		log.Fatalf("Failed to instantiate a Token contract: %v", err)
	}
	//fmt.Println("contract token======>:", token)

	// 开始读取链上数据

	//fmt.Println("================get plmnid from blockchain==================")
	//从以太坊上获得之前存进去的plmnid
	plmnid, _ := token.GetPLMNID(&bind.CallOpts{Pending: true, From: corenetworkaddr}, supi)
	//fmt.Println("plmnid is:", string(plmnid))

	//fmt.Println("================get Data_Network_Name from blockchain==================")
	//从以太坊上获得之前存进去的值Data_Network_Name
	data_Network_Name, _ := token.GetDataNetworkName(&bind.CallOpts{Pending: true, From: corenetworkaddr}, supi)
	//fmt.Println("data_Network_Name is:", string(data_Network_Name))

	//fmt.Println("================get uplinkAMBR from blockchain==================")
	//从以太坊上获得之前存进去的值uplinkAMBR
	uplinkAMBR, _ := token.GetUplinkAMBR(&bind.CallOpts{Pending: true, From: corenetworkaddr}, supi)
	//fmt.Println("uplinkAMBR is:", string(uplinkAMBR))

	//fmt.Println("================get downlinkAMBR from blockchain==================")
	//从以太坊上获得之前存进去的值uplinkAMBR
	downlinkAMBR, _ := token.GetDownlinkAMBR(&bind.CallOpts{Pending: true, From: corenetworkaddr}, supi)
	//fmt.Println("downlinkAMBR is:", string(downlinkAMBR))

	//fmt.Println("================get sst from blockchain==================")
	//从以太坊上获得之前存进去的值sst
	sst, _ := token.GetSST(&bind.CallOpts{Pending: true, From: corenetworkaddr}, supi)
	//fmt.Println("sst is:", string(sst))

	//fmt.Println("================get sd from blockchain==================")
	//从以太坊上获得之前存进去的值sd
	sd, _ := token.GetSD(&bind.CallOpts{Pending: true, From: corenetworkaddr}, supi)
	//fmt.Println("sd is:", string(sd))

	// "subscriptionData.provisionedData.amData"
	var amSubsData models.AccessAndMobilitySubscriptionData
	tmp0 := bson.M{}
	bisonData1 := `
	{
		"gpsis": ["msisdn-0900000000"],
		"subscribedUeAmbr": {
			"uplink": "` + uplinkAMBR + `",
			"downlink": "` + downlinkAMBR + `"
		},
		"nssai": {
			"defaultSingleNssais": [{
				"sd": "` + sd + `",
				"sst": ` + sst + `
			}]
		},
		"ueId": "` + ue.Supi + `",
		"servingPlmnId": "` + plmnid + `"
	}
	`
	// 原本 nssai 如下，但是 多切片 没确定，先一个着
	// "nssai": {
	// 	"defaultSingleNssais": [{
	// 		"sd": "010203",
	// 		"sst": 1
	// 	}, {
	// 		"sd": "112233",
	// 		"sst": 1
	// 	}]
	// },
	bson.UnmarshalExtJSON([]byte(bisonData1), true, &tmp0)
	mapstructure.Decode(tmp0, &amSubsData)
	ue.AccessAndMobilitySubscriptionData = &amSubsData
	ue.Gpsi = amSubsData.Gpsis[0]

	// "subscriptionData.provisionedData.smfSelectionSubscriptionData"
	var smfSelectionSubscriptionData models.SmfSelectionSubscriptionData
	tmp1 := bson.M{}
	bsonContext1 := `
	{
		"_id": ObjectId("60ed459cf8907fde962cf3ad"),
		"subscribedSnssaiInfos": {
			"` + sst + sd + `": {
				"dnnInfos": [{
					"dnn": "` + data_Network_Name + `"
				}]
			},
		},
		"ueId": "` + ue.Supi + `",
		"servingPlmnId": "` + plmnid + `"
	}
	`
	// 原本如下，多 dnn 多切片 都没搞清，先一个着
	// "01112233": {
	// 	"dnnInfos": [{
	// 		"dnn": "internet"
	// 	}, {
	// 		"dnn": "internet2"
	// 	}]
	// }
	bson.UnmarshalExtJSON([]byte(bsonContext1), true, &tmp1)
	mapstructure.Decode(tmp1, &smfSelectionSubscriptionData)
	ue.SmfSelectionData = &smfSelectionSubscriptionData

	// By uuuuu - end

	ue.ContextValid = true
	return nil
}

func getSubscribedNssai(ue *context.AmfUe) {

	// By uuuuu - entry
	// 下面开始从区块链读入 切片信息，原操作为 读取"subscriptionData.provisionedData.amData"
	// 再存入 "subscriptionData.contextData.amf3gppAccess"，是 model 的 type Amf3GppAccessRegistration struct
	// 此处去掉了存入过程，仅读取

	// defer，eth client 关闭
	defer client.Close()

	// 合约地址
	contractAddress_byte, err := ioutil.ReadFile("./eth_contract_address/contractAddress.txt")
	if err != nil {
		fmt.Println("read contractAddress failed", err)
	}
	contractAddress := string(contractAddress_byte)

	// 核心网节点地址
	corenetworkAddress_byte, err := ioutil.ReadFile("./eth_contract_address/corenetworkAddress.txt")
	if err != nil {
		fmt.Println("read corenetworkAddress failed", err)
	}
	corenetworkAddress := string(corenetworkAddress_byte)
	corenetworkaddr := common.HexToAddress(corenetworkAddress)

	//fmt.Println("=========+++++++++++++建立golang与链的连接+++++++++++++=========")
	token, err := jianquan.NewMain(common.HexToAddress(contractAddress), client)
	if err != nil {
		log.Fatalf("Failed to instantiate a Token contract: %v", err)
	}
	//fmt.Println("contract token======>:", token)
	supi2, _ := strconv.ParseInt(ue.Supi[5:], 10, 64)
	supi := big.NewInt(supi2)

	// 开始读取链上数据

	//fmt.Println("================get sst from blockchain==================")
	//从以太坊上获得之前存进去的值sst
	sst, _ := token.GetSST(&bind.CallOpts{Pending: true, From: corenetworkaddr}, supi)
	//fmt.Println("sst is:", string(sst))

	//fmt.Println("================get sd from blockchain==================")
	//从以太坊上获得之前存进去的值sd
	sd, _ := token.GetSD(&bind.CallOpts{Pending: true, From: corenetworkaddr}, supi)
	//fmt.Println("sd is:", string(sd))

	//fmt.Println("================get Default_S_NSSAI from blockchain==================")
	//从以太坊上获得之前存进去的值Default_S_NSSAI
	default_S_NSSAI, _ := token.GetDefaultSNSSAI(&bind.CallOpts{Pending: true, From: corenetworkaddr}, supi)
	//fmt.Println("default_S_NSSAI is:", default_S_NSSAI)

	//fmt.Println("================get Data_Network_Name from blockchain==================")
	//从以太坊上获得之前存进去的值Data_Network_Name
	//data_Network_Name, _ := token.GetDataNetworkName(&bind.CallOpts{Pending: true, From: corenetworkaddr}, supi)
	//fmt.Println("data_Network_Name is:", string(data_Network_Name))

	sst1, _ := strconv.ParseInt(sst, 10, 32)
	subscribedSnssai := models.SubscribedSnssai{
		SubscribedSnssai: &models.Snssai{
			Sst: int32(sst1),
			Sd:  sd,
		},
		DefaultIndication: default_S_NSSAI,
	}
	ue.SubscribedNssai = append(ue.SubscribedNssai, subscribedSnssai)

	// 下面是 多个 nssai 的解决方法
	// subscribedSnssai = models.SubscribedSnssai{
	// 	SubscribedSnssai: &models.Snssai{
	// 		Sst: 1,
	// 		Sd:  "112233",
	// 	},
	// 	DefaultIndication: true,
	// }
	// ue.SubscribedNssai = append(ue.SubscribedNssai, subscribedSnssai)

	// By uuuuu - end

}

// TS 23.502 4.2.2.2.3 Registration with AMF Re-allocation
func handleRequestedNssai(ue *context.AmfUe, anType models.AccessType) error {
	amfSelf := context.AMF_Self()

	if ue.RegistrationRequest.RequestedNSSAI != nil {
		requestedNssai, err := nasConvert.RequestedNssaiToModels(ue.RegistrationRequest.RequestedNSSAI)
		if err != nil {
			return fmt.Errorf("Decode failed at RequestedNSSAI[%s]", err)
		}

		ue.GmmLog.Debugf("RequestedNssai: %+v", requestedNssai)

		needSliceSelection := false
		for _, requestedSnssai := range requestedNssai {
			if ue.InSubscribedNssai(*requestedSnssai.ServingSnssai) {
				allowedSnssai := models.AllowedSnssai{
					AllowedSnssai: &models.Snssai{
						Sst: requestedSnssai.ServingSnssai.Sst,
						Sd:  requestedSnssai.ServingSnssai.Sd,
					},
					MappedHomeSnssai: requestedSnssai.HomeSnssai,
				}
				ue.AllowedNssai[anType] = append(ue.AllowedNssai[anType], allowedSnssai)
			} else {
				needSliceSelection = true
				break
			}
		}

		if needSliceSelection {
			if ue.NssfUri == "" {
				for {
					err := consumer.SearchNssfNSSelectionInstance(ue, amfSelf.NrfUri, models.NfType_NSSF, models.NfType_AMF, nil)
					if err != nil {
						ue.GmmLog.Errorf("AMF can not select an NSSF Instance by NRF[Error: %+v]", err)
						time.Sleep(2 * time.Second)
					} else {
						break
					}
				}
			}

			// Step 4
			problemDetails, err := consumer.NSSelectionGetForRegistration(ue, requestedNssai)
			if problemDetails != nil {
				ue.GmmLog.Errorf("NSSelection Get Failed Problem[%+v]", problemDetails)
				gmm_message.SendRegistrationReject(ue.RanUe[anType], nasMessage.Cause5GMMProtocolErrorUnspecified, "")
				return fmt.Errorf("Handle Requested Nssai of UE failed")
			} else if err != nil {
				ue.GmmLog.Errorf("NSSelection Get Error[%+v]", err)
				gmm_message.SendRegistrationReject(ue.RanUe[anType], nasMessage.Cause5GMMProtocolErrorUnspecified, "")
				return fmt.Errorf("Handle Requested Nssai of UE failed")
			}

			// Step 5: Initial AMF send Namf_Communication_RegistrationCompleteNotify to old AMF
			req := models.UeRegStatusUpdateReqData{
				TransferStatus: models.UeContextTransferStatus_NOT_TRANSFERRED,
			}
			_, problemDetails, err = consumer.RegistrationStatusUpdate(ue, req)
			if problemDetails != nil {
				ue.GmmLog.Errorf("Registration Status Update Failed Problem[%+v]", problemDetails)
			} else if err != nil {
				ue.GmmLog.Errorf("Registration Status Update Error[%+v]", err)
			}

			// Step 6
			searchTargetAmfQueryParam := Nnrf_NFDiscovery.SearchNFInstancesParamOpts{}
			if ue.NetworkSliceInfo != nil {
				netwotkSliceInfo := ue.NetworkSliceInfo
				if netwotkSliceInfo.TargetAmfSet != "" {
					// TS 29.531
					// TargetAmfSet format: ^[0-9]{3}-[0-9]{2-3}-[A-Fa-f0-9]{2}-[0-3][A-Fa-f0-9]{2}$
					// mcc-mnc-amfRegionId(8 bit)-AmfSetId(10 bit)
					targetAmfSetToken := strings.Split(netwotkSliceInfo.TargetAmfSet, "-")
					guami := amfSelf.ServedGuamiList[0]
					targetAmfPlmnId := models.PlmnId{
						Mcc: targetAmfSetToken[0],
						Mnc: targetAmfSetToken[1],
					}

					if !reflect.DeepEqual(guami.PlmnId, targetAmfPlmnId) {
						searchTargetAmfQueryParam.TargetPlmnList =
							optional.NewInterface(util.MarshToJsonString([]models.PlmnId{targetAmfPlmnId}))
						searchTargetAmfQueryParam.RequesterPlmnList =
							optional.NewInterface(util.MarshToJsonString([]models.PlmnId{*guami.PlmnId}))
					}

					searchTargetAmfQueryParam.AmfRegionId = optional.NewString(targetAmfSetToken[2])
					searchTargetAmfQueryParam.AmfSetId = optional.NewString(targetAmfSetToken[3])
				} else if len(netwotkSliceInfo.CandidateAmfList) > 0 {
					// TODO: select candidate Amf based on local poilcy
					searchTargetAmfQueryParam.TargetNfInstanceId = optional.NewInterface(netwotkSliceInfo.CandidateAmfList[0])
				}
			}

			err = consumer.SearchAmfCommunicationInstance(ue, amfSelf.NrfUri,
				models.NfType_AMF, models.NfType_AMF, &searchTargetAmfQueryParam)
			if err == nil {
				// Condition (A) Step 7: initial AMF find Target AMF via NRF ->
				// Send Namf_Communication_N1MessageNotify to Target AMF
				ueContext := consumer.BuildUeContextModel(ue)
				registerContext := models.RegistrationContextContainer{
					UeContext:        &ueContext,
					AnType:           anType,
					AnN2ApId:         int32(ue.RanUe[anType].RanUeNgapId),
					RanNodeId:        ue.RanUe[anType].Ran.RanId,
					InitialAmfName:   amfSelf.Name,
					UserLocation:     &ue.Location,
					RrcEstCause:      ue.RanUe[anType].RRCEstablishmentCause,
					UeContextRequest: ue.RanUe[anType].UeContextRequest,
					AnN2IPv4Addr:     ue.RanUe[anType].Ran.Conn.RemoteAddr().String(),
					AllowedNssai: &models.AllowedNssai{
						AllowedSnssaiList: ue.AllowedNssai[anType],
						AccessType:        anType,
					},
				}
				if len(ue.NetworkSliceInfo.RejectedNssaiInPlmn) > 0 {
					registerContext.RejectedNssaiInPlmn = ue.NetworkSliceInfo.RejectedNssaiInPlmn
				}
				if len(ue.NetworkSliceInfo.RejectedNssaiInTa) > 0 {
					registerContext.RejectedNssaiInTa = ue.NetworkSliceInfo.RejectedNssaiInTa
				}

				var n1Message bytes.Buffer
				ue.RegistrationRequest.EncodeRegistrationRequest(&n1Message)
				callback.SendN1MessageNotifyAtAMFReAllocation(ue, n1Message.Bytes(), &registerContext)
			} else {
				// Condition (B) Step 7: initial AMF can not find Target AMF via NRF -> Send Reroute NAS Request to RAN
				allowedNssaiNgap := ngapConvert.AllowedNssaiToNgap(ue.AllowedNssai[anType])
				ngap_message.SendRerouteNasRequest(ue, anType, nil, ue.RanUe[anType].InitialUEMessage, &allowedNssaiNgap)
			}
			return nil
		}
	}

	// if registration request has no requested nssai, or non of snssai in requested nssai is permitted by nssf
	// then use ue subscribed snssai which is marked as default as allowed nssai
	if len(ue.AllowedNssai[anType]) == 0 {
		for _, snssai := range ue.SubscribedNssai {
			if snssai.DefaultIndication {
				if amfSelf.InPlmnSupportList(*snssai.SubscribedSnssai) {
					allowedSnssai := models.AllowedSnssai{
						AllowedSnssai: snssai.SubscribedSnssai,
					}
					ue.AllowedNssai[anType] = append(ue.AllowedNssai[anType], allowedSnssai)
				}
			}
		}
	}
	return nil
}

func assignLadnInfo(ue *context.AmfUe, accessType models.AccessType) {
	amfSelf := context.AMF_Self()

	ue.LadnInfo = nil
	if ue.RegistrationRequest.LADNIndication != nil {
		// request for LADN information
		if ue.RegistrationRequest.LADNIndication.GetLen() == 0 {
			if ue.HasWildCardSubscribedDNN() {
				for _, ladn := range amfSelf.LadnPool {
					if ue.TaiListInRegistrationArea(ladn.TaiLists, accessType) {
						ue.LadnInfo = append(ue.LadnInfo, *ladn)
					}
				}
			} else {
				for _, snssaiInfos := range ue.SmfSelectionData.SubscribedSnssaiInfos {
					for _, dnnInfo := range snssaiInfos.DnnInfos {
						if ladn, ok := amfSelf.LadnPool[dnnInfo.Dnn]; ok { // check if this dnn is a ladn
							if ue.TaiListInRegistrationArea(ladn.TaiLists, accessType) {
								ue.LadnInfo = append(ue.LadnInfo, *ladn)
							}
						}
					}
				}
			}
		} else {
			requestedLadnList := nasConvert.LadnToModels(ue.RegistrationRequest.LADNIndication.GetLADNDNNValue())
			for _, requestedLadn := range requestedLadnList {
				if ladn, ok := amfSelf.LadnPool[requestedLadn]; ok {
					if ue.TaiListInRegistrationArea(ladn.TaiLists, accessType) {
						ue.LadnInfo = append(ue.LadnInfo, *ladn)
					}
				}
			}
		}
	} else if ue.SmfSelectionData != nil {
		for _, snssaiInfos := range ue.SmfSelectionData.SubscribedSnssaiInfos {
			for _, dnnInfo := range snssaiInfos.DnnInfos {
				if dnnInfo.Dnn != "*" {
					if ladn, ok := amfSelf.LadnPool[dnnInfo.Dnn]; ok {
						if ue.TaiListInRegistrationArea(ladn.TaiLists, accessType) {
							ue.LadnInfo = append(ue.LadnInfo, *ladn)
						}
					}
				}
			}
		}
	}
}

func HandleIdentityResponse(ue *context.AmfUe, identityResponse *nasMessage.IdentityResponse) error {
	if ue == nil {
		return fmt.Errorf("AmfUe is nil")
	}

	ue.GmmLog.Info("Handle Identity Response")

	mobileIdentityContents := identityResponse.MobileIdentity.GetMobileIdentityContents()
	switch nasConvert.GetTypeOfIdentity(mobileIdentityContents[0]) { // get type of identity
	case nasMessage.MobileIdentity5GSTypeSuci:
		var plmnId string
		ue.Suci, plmnId = nasConvert.SuciToString(mobileIdentityContents)
		ue.PlmnId = util.PlmnIdStringToModels(plmnId)
		ue.GmmLog.Debugf("get SUCI: %s", ue.Suci)
	case nasMessage.MobileIdentity5GSType5gGuti:
		if ue.MacFailed {
			return fmt.Errorf("NAS message integrity check failed")
		}
		_, guti := nasConvert.GutiToString(mobileIdentityContents)
		ue.Guti = guti
		ue.GmmLog.Debugf("get GUTI: %s", guti)
	case nasMessage.MobileIdentity5GSType5gSTmsi:
		if ue.MacFailed {
			return fmt.Errorf("NAS message integrity check failed")
		}
		sTmsi := hex.EncodeToString(mobileIdentityContents[1:])
		if tmp, err := strconv.ParseInt(sTmsi[4:], 10, 32); err != nil {
			return err
		} else {
			ue.Tmsi = int32(tmp)
		}
		ue.GmmLog.Debugf("get 5G-S-TMSI: %s", sTmsi)
	case nasMessage.MobileIdentity5GSTypeImei:
		if ue.MacFailed {
			return fmt.Errorf("NAS message integrity check failed")
		}
		imei := nasConvert.PeiToString(mobileIdentityContents)
		ue.Pei = imei
		ue.GmmLog.Debugf("get PEI: %s", imei)
	case nasMessage.MobileIdentity5GSTypeImeisv:
		if ue.MacFailed {
			return fmt.Errorf("NAS message integrity check failed")
		}
		imeisv := nasConvert.PeiToString(mobileIdentityContents)
		ue.Pei = imeisv
		ue.GmmLog.Debugf("get PEI: %s", imeisv)
	}
	return nil
}

// TS 24501 5.6.3.2
func HandleNotificationResponse(ue *context.AmfUe, notificationResponse *nasMessage.NotificationResponse) error {
	ue.GmmLog.Info("Handle Notification Response")

	if ue.MacFailed {
		return fmt.Errorf("NAS message integrity check failed")
	}

	if ue.T3565 != nil {
		ue.T3565.Stop()
		ue.T3565 = nil // clear the timer
	}

	if notificationResponse != nil && notificationResponse.PDUSessionStatus != nil {
		psiArray := nasConvert.PSIToBooleanArray(notificationResponse.PDUSessionStatus.Buffer)
		for psi := 1; psi <= 15; psi++ {
			pduSessionId := int32(psi)
			if smContext, ok := ue.SmContextFindByPDUSessionID(pduSessionId); ok {
				if !psiArray[psi] {
					cause := models.Cause_PDU_SESSION_STATUS_MISMATCH
					causeAll := &context.CauseAll{
						Cause: &cause,
					}
					problemDetail, err := consumer.SendReleaseSmContextRequest(ue, smContext, causeAll, "", nil)
					if problemDetail != nil {
						ue.GmmLog.Errorf("Release SmContext Failed Problem[%+v]", problemDetail)
					} else if err != nil {
						ue.GmmLog.Errorf("Release SmContext Error[%v]", err.Error())
					}
				}
			}
		}
	}
	return nil
}

func HandleConfigurationUpdateComplete(ue *context.AmfUe,
	configurationUpdateComplete *nasMessage.ConfigurationUpdateComplete) error {
	ue.GmmLog.Info("Handle Configuration Update Complete")

	if ue.MacFailed {
		return fmt.Errorf("NAS message integrity check failed")
	}

	// TODO: Stop timer T3555 in TS 24.501 Figure 5.4.4.1.1 in handler
	// TODO: Send acknowledgment by Nudm_SMD_Info_Service to UDM in handler
	//		import "github.com/free5gc/openapi/Nudm_SubscriberDataManagement" client.Info

	return nil
}

func calNewKey(RAND []byte, pubK string) (newKey []byte) {

	saddr = "0xe18151539fef9799b4d003707e94a69fb9aee3bd"
	//fmt.Println("请输入账户地址：")
	//fmt.Scanln(&saddr)
	addr := common.HexToAddress(saddr)
	account, err := ks.GetAccount(addr)
	if err != nil {
		fmt.Println("err = ", err)
		os.Exit(0)
	}

	//pubK := "030756947a18ffe304e86038e2b3946010283bb79debaf4d997d81842e9beecaa3"
	compressPublicKey, _ := hex.DecodeString(pubK)
	decompressPub, _ := ethcrypto.DecompressPubkey(compressPublicKey)

	//fmt.Printf("RAND：%#v\n", RAND)
	RAND256 := append(RAND, RAND...)

	newKey = ks.GetNewKey(account, decompressPub, RAND256)

	return newKey
}

func AuthenticationProcedure(ue *context.AmfUe, accessType models.AccessType) (bool, error) {
	ue.GmmLog.Info("Authentication procedure")

	// Check whether UE has SUCI and SUPI
	if IdentityVerification(ue) {
		ue.GmmLog.Debugln("UE has SUCI / SUPI")
		if ue.SecurityContextIsValid() {
			ue.GmmLog.Debugln("UE has a valid security context - skip the authentication procedure")
			return true, nil
		}
	} else {
		// Request UE's SUCI by sending identity request
		gmm_message.SendIdentityRequest(ue.RanUe[accessType], nasMessage.MobileIdentity5GSTypeSuci)
		return false, nil
	}

	// By uuuuu - entry
	// 原本将选择 AUSF，并调用 consumer.SendUEAuthenticationAuthenticateRequest(ue, nil)
	amfSelf := context.AMF_Self()
	servedGuami := amfSelf.ServedGuamiList[0]

	var authInfo models.AuthenticationInfo

	// 此处直接确定使用的是 suci，一大串数是 udmProfileAHNPrivateKey，ToSupi 是用它解的，在 udmcfg.yaml 里给出
	authInfo.SupiOrSuci = ue.Suci
	supiimsi, err := suci.ToSupi(authInfo.SupiOrSuci, "c53c22208b61860b06c62e5406a7b330c2b577aa5558981510d128247d38bd1d")
	supi := supiimsi[5:]
	//fmt.Println("Supi: ", supi)
	if err != nil {
		fmt.Println("\nsuci->supi chucuola!!!\n")
		return false, err
	}

	mnc, _ := strconv.Atoi(servedGuami.PlmnId.Mnc)
	authInfo.ServingNetworkName = fmt.Sprintf("5G:mnc%03d.mcc%s.3gppnetwork.org", mnc, servedGuami.PlmnId.Mcc)

	// 下面是原本的 AUSF 连接 UDM，再到 UDR，取 mongo 的 subscriptionData.authenticationData.authenticationSubscription
	// 简化为从区块链上读取 key
	// 中间是 UDM 的 func GenerateAuthDataProcedure 产生 AUTN，这部分基本照搬

	// defer，eth client 关闭
	defer client.Close()

	// 合约地址
	contractAddress_byte, err := ioutil.ReadFile("./eth_contract_address/contractAddress.txt")
	if err != nil {
		fmt.Println("read contractAddress failed", err)
	}
	contractAddress := string(contractAddress_byte)

	// 核心网节点地址
	corenetworkAddress_byte, err := ioutil.ReadFile("./eth_contract_address/corenetworkAddress.txt")
	if err != nil {
		fmt.Println("read corenetworkAddress failed", err)
	}
	corenetworkAddress := string(corenetworkAddress_byte)
	corenetworkaddr := common.HexToAddress(corenetworkAddress)

	//fmt.Println("=========+++++++++++++建立golang与链的连接+++++++++++++=========")
	token, err := jianquan.NewMain(common.HexToAddress(contractAddress), client)
	if err != nil {
		log.Fatalf("Failed to instantiate a Token contract: %v", err)
	}
	//fmt.Println("contract token======>:", token)

	// 开始读取链上数据
	supi2, _ := strconv.ParseInt(supi, 10, 64)
	supi3 := big.NewInt(supi2)

	//fmt.Println("================get supi from blockchain==================")
	//从以太坊上获得之前存进去的supi
	querysupi, _ := token.GetSUPI(&bind.CallOpts{Pending: true, From: corenetworkaddr}, supi3)
	//fmt.Println("supi is:", querysupi)
	cmpRe := querysupi.Cmp(supi3)
	if cmpRe != 0 {
		//fmt.Println("\nsupi he eth supi buyiyanga!!!\n")
		return false, err
	}
	ue.Supi = supiimsi

	var opStr string
	// k, op, opc := make([]byte, 16), make([]byte, 16), make([]byte, 16)
	k, opc := make([]byte, 16), make([]byte, 16)

	const (
		SqnMAx    int64 = 0x7FFFFFFFFFF
		ind       int64 = 32
		keyStrLen int   = 32
		opStrLen  int   = 32
		opcStrLen int   = 32
	)

	RAND := make([]byte, 16)
	cryptoRand.Read(RAND)
	//fmt.Printf("RAND：%#v\n", RAND)

	var sqnGenerator sqnGen
	sqnGenerator.rand = RAND

	// 从链上获取 key
	//fmt.Println("================get pk from blockchain==================")
	//从以太坊上获得之前存进去的pk
	pk, _ := token.GetPK(&bind.CallOpts{Pending: true, From: corenetworkaddr}, supi3)
	//fmt.Println("pk is:", string(pk))
	//fmt.Printf("get pk from blockchain：%#v\n", string(pk))
	ausf_logger.Auth5gAkaComfirmLog.Infof("Get public key from blockchain：0x%s\n", string(pk))
	ausf_logger.Auth5gAkaComfirmLog.Infof("Generating k for auth...")
	k, _ = hex.DecodeString(pk)
	sqnGenerator.k = k
	// k = calNewKey 计算 sk*pk，采用非对称密钥
	//	k = calNewKey(RAND, pk)
	//fmt.Printf("Generate k：%#v\n", k)
	ausf_logger.Auth5gAkaComfirmLog.Infof("Generated k: 0x%v\n", hex.EncodeToString(k))

	//fmt.Println("================get plmnid from blockchain==================")
	// 从以太坊上获得之前存进去的plmnid
	// plmnid, _ := token.GetPLMNID(&bind.CallOpts{Pending: true, From: corenetworkaddr}, supi3)
	//fmt.Println("plmnid is:", string(plmnid))

	//fmt.Println("================get au_method from blockchain==================")
	//从以太坊上获得之前存进去的au_method
	au_method, _ := token.GetAuMethod(&bind.CallOpts{Pending: true, From: corenetworkaddr}, supi3)
	//fmt.Println("au_method is:", string(au_method))

	//fmt.Println("================get operator code type from blockchain==================")
	//从以太坊上获得之前存进去的oct
	// oct, _ := token.GetOCT(&bind.CallOpts{Pending: true, From: corenetworkaddr}, supi3)
	//fmt.Println("oct is:", string(oct))
	// 只支持 OPc

	//fmt.Println("================get operator code value from blockchain==================")
	//从以太坊上获得之前存进去的值ocv
	opStr, _ = token.GetOCV(&bind.CallOpts{Pending: true, From: corenetworkaddr}, supi3)
	//fmt.Println("ocv is:", string(opStr))

	opc, _ = hex.DecodeString(opStr)
	sqnGenerator.opc = opc
	mapForSqn[ue.Supi] = &sqnGenerator
	//	opc, _ = milenage.GenerateOPC(k, op)

	// SQN 在 webconsole 的生成方式
	// authSubs_sequenceNumber := "16f3b3f70fc2"
	// authSubs["sequenceNumber"].(string)
	// 在 UDR 里请求鉴权内容时，会自增 1，每个 UE 有不同的 SQN
	var authSubs_sequenceNumber string
	authSubs_sequenceNumber = "000000000000"

	// 下面开始照搬 udm/producer/generate_auth_data.go
	sqnStr := strictHex(authSubs_sequenceNumber, 12)
	sqn, _ := hex.DecodeString(sqnStr)

	AMF, err := hex.DecodeString("8000")

	bigSQN := big.NewInt(0)
	sqn, err = hex.DecodeString(sqnStr)

	bigSQN.SetString(sqnStr, 16)

	bigInc := big.NewInt(1)
	bigSQN = bigInc.Add(bigSQN, bigInc)

	SQNheStr := fmt.Sprintf("%x", bigSQN)
	SQNheStr = strictHex(SQNheStr, 12)

	macA, macS := make([]byte, 8), make([]byte, 8)
	CK, IK := make([]byte, 16), make([]byte, 16)
	RES := make([]byte, 8)
	AK, AKstar := make([]byte, 6), make([]byte, 6)

	milenage.F1(opc, k, RAND, sqn, AMF, macA, macS)
	milenage.F2345(opc, k, RAND, RES, CK, IK, AK, AKstar)

	SQNxorAK := make([]byte, 6)
	for i := 0; i < len(sqn); i++ {
		SQNxorAK[i] = sqn[i] ^ AK[i]
	}

	AUTN := append(append(SQNxorAK, AMF...), macA...)
	//fmt.Printf("AUTN = %x\n", AUTN)

	var av models.AuthenticationVector
	var am models.AuthType

	// 默认鉴权方式 5G_AKA，此处继续照搬
	if models.AuthMethod(au_method) == models.AuthMethod__5_G_AKA {
		am = models.AuthType__5_G_AKA
		// derive XRES*
		key := append(CK, IK...)
		FC := UeauCommon.FC_FOR_RES_STAR_XRES_STAR_DERIVATION
		P0 := []byte(authInfo.ServingNetworkName)
		P1 := RAND
		P2 := RES

		kdfValForXresStar := UeauCommon.GetKDFValue(
			key, FC, P0, UeauCommon.KDFLen(P0), P1, UeauCommon.KDFLen(P1), P2, UeauCommon.KDFLen(P2))
		xresStar := kdfValForXresStar[len(kdfValForXresStar)/2:]

		// derive Kausf
		FC = UeauCommon.FC_FOR_KAUSF_DERIVATION
		P0 = []byte(authInfo.ServingNetworkName)
		P1 = SQNxorAK
		kdfValForKausf := UeauCommon.GetKDFValue(key, FC, P0, UeauCommon.KDFLen(P0), P1, UeauCommon.KDFLen(P1))
		// fmt.Printf("Kausf = %x\n", kdfValForKausf)

		// Fill in rand, xresStar, autn, kausf
		av.Rand = hex.EncodeToString(RAND)
		av.XresStar = hex.EncodeToString(xresStar)
		av.Autn = hex.EncodeToString(AUTN)
		av.Kausf = hex.EncodeToString(kdfValForKausf)
	} else { // EAP-AKA'
		// response.AuthType = models.AuthType_EAP_AKA_PRIME
		am = models.AuthType_EAP_AKA_PRIME
		// derive CK' and IK'
		key := append(CK, IK...)
		FC := UeauCommon.FC_FOR_CK_PRIME_IK_PRIME_DERIVATION
		P0 := []byte(authInfo.ServingNetworkName)
		P1 := SQNxorAK
		kdfVal := UeauCommon.GetKDFValue(key, FC, P0, UeauCommon.KDFLen(P0), P1, UeauCommon.KDFLen(P1))
		// fmt.Printf("kdfVal = %x (len = %d)\n", kdfVal, len(kdfVal))

		// For TS 35.208 test set 19 & RFC 5448 test vector 1
		// CK': 0093 962d 0dd8 4aa5 684b 045c 9edf fa04
		// IK': ccfc 230c a74f cc96 c0a5 d611 64f5 a76

		ckPrime := kdfVal[:len(kdfVal)/2]
		ikPrime := kdfVal[len(kdfVal)/2:]
		// fmt.Printf("ckPrime: %x\nikPrime: %x\n", ckPrime, ikPrime)

		// Fill in rand, xres, autn, ckPrime, ikPrime
		av.Rand = hex.EncodeToString(RAND)
		av.Xres = hex.EncodeToString(RES)
		av.Autn = hex.EncodeToString(AUTN)
		av.CkPrime = hex.EncodeToString(ckPrime)
		av.IkPrime = hex.EncodeToString(ikPrime)
	}

	// UDM 部分结束，进入 ausf/producer/ue_authentication.go 开始照搬
	var response models.UeAuthenticationCtx
	response.ServingNetworkName = authInfo.ServingNetworkName

	// 此处并没有做 ausf_context.AddSuciSupiPairToMap(authInfo.SupiOrSuci, ueId)
	// ausf ue context 直接去掉，不存在了

	if am == models.AuthType__5_G_AKA {
		// Derive HXRES* from XRES*
		concat := av.Rand + av.XresStar
		var hxresStarBytes []byte

		bytes, _ := hex.DecodeString(concat)
		hxresStarBytes = bytes
		hxresStarAll := sha256.Sum256(hxresStarBytes)
		hxresStar := hex.EncodeToString(hxresStarAll[16:]) // last 128 bits

		// Derive Kseaf from Kausf
		Kausf := av.Kausf
		var KausfDecode []byte
		ausfDecode, _ := hex.DecodeString(Kausf)
		KausfDecode = ausfDecode

		P0 := []byte(authInfo.ServingNetworkName)

		Kseaf[ue.Supi] = UeauCommon.GetKDFValue(KausfDecode, UeauCommon.FC_FOR_KSEAF_DERIVATION, P0, UeauCommon.KDFLen(P0))

		var av5gAka models.Av5gAka
		av5gAka.Rand = av.Rand
		av5gAka.Autn = av.Autn
		av5gAka.HxresStar = hxresStar

		response.Var5gAuthData = av5gAka
	} else if am == models.AuthType_EAP_AKA_PRIME {
		fmt.Println("EAP not done!")
		return false, nil
	}

	response.AuthType = am

	// 自己加了个全局 map，取代 ausfcontext 来完成 xres 的比较
	xresForConfirm[ue.Supi] = av.XresStar
	ue.AuthenticationCtx = &response
	ue.ABBA = []uint8{0x00, 0x00} // set ABBA value as described at TS 33.501 Annex A.7.1

	gmm_message.SendAuthenticationRequest(ue.RanUe[accessType])
	//fmt.Println("\n\nAUTH Request Sent.\n")

	// By uuuuu - end

	return false, nil
}

// TS 24501 5.6.1
func HandleServiceRequest(ue *context.AmfUe, anType models.AccessType,
	serviceRequest *nasMessage.ServiceRequest) error {
	if ue == nil {
		return fmt.Errorf("AmfUe is nil")
	}

	ue.GmmLog.Info("Handle Service Request")

	if ue.T3513 != nil {
		ue.T3513.Stop()
		ue.T3513 = nil // clear the timer
	}
	if ue.T3565 != nil {
		ue.T3565.Stop()
		ue.T3565 = nil // clear the timer
	}

	// Send Authtication / Security Procedure not support
	if !ue.SecurityContextIsValid() {
		ue.GmmLog.Warnf("No Security Context : SUPI[%s]", ue.Supi)
		gmm_message.SendServiceReject(ue.RanUe[anType], nil, nasMessage.Cause5GMMUEIdentityCannotBeDerivedByTheNetwork)
		ngap_message.SendUEContextReleaseCommand(ue.RanUe[anType],
			context.UeContextN2NormalRelease, ngapType.CausePresentNas, ngapType.CauseNasPresentNormalRelease)
		return nil
	}

	// TS 24.501 8.2.6.21: if the UE is sending a REGISTRATION REQUEST message as an initial NAS message,
	// the UE has a valid 5G NAS security context and the UE needs to send non-cleartext IEs
	// TS 24.501 4.4.6: When the UE sends a REGISTRATION REQUEST or SERVICE REQUEST message that includes a NAS message
	// container IE, the UE shall set the security header type of the initial NAS message to "integrity protected"
	if serviceRequest.NASMessageContainer != nil {
		contents := serviceRequest.NASMessageContainer.GetNASMessageContainerContents()

		// TS 24.501 4.4.6: When the UE sends a REGISTRATION REQUEST or SERVICE REQUEST message that includes a NAS
		// message container IE, the UE shall set the security header type of the initial NAS message to
		// "integrity protected"; then the AMF shall decipher the value part of the NAS message container IE
		err := security.NASEncrypt(ue.CipheringAlg, ue.KnasEnc, ue.ULCount.Get(), security.Bearer3GPP,
			security.DirectionUplink, contents)

		if err != nil {
			ue.SecurityContextAvailable = false
		} else {
			m := nas.NewMessage()
			if err := m.GmmMessageDecode(&contents); err != nil {
				return err
			}

			messageType := m.GmmMessage.GmmHeader.GetMessageType()
			if messageType != nas.MsgTypeServiceRequest {
				return errors.New("The payload of NAS message Container is not service request")
			}
			// TS 24.501 4.4.6: The AMF shall consider the NAS message that is obtained from the NAS message container
			// IE as the initial NAS message that triggered the procedure
			serviceRequest = m.ServiceRequest
		}
		// TS 33.501 6.4.6 step 3: if the initial NAS message was protected but did not pass the integrity check
		ue.RetransmissionOfInitialNASMsg = ue.MacFailed
	}

	serviceType := serviceRequest.GetServiceTypeValue()
	var reactivationResult, acceptPduSessionPsi *[16]bool
	var errPduSessionId, errCause []uint8
	var targetPduSessionId int32
	suList := ngapType.PDUSessionResourceSetupListSUReq{}
	ctxList := ngapType.PDUSessionResourceSetupListCxtReq{}

	if serviceType == nasMessage.ServiceTypeEmergencyServices ||
		serviceType == nasMessage.ServiceTypeEmergencyServicesFallback {
		ue.GmmLog.Warnf("emergency service is not supported")
	}

	if serviceType == nasMessage.ServiceTypeSignalling {
		err := sendServiceAccept(ue, anType, ctxList, suList, nil, nil, nil, nil)
		return err
	}
	if ue.N1N2Message != nil {
		requestData := ue.N1N2Message.Request.JsonData
		if ue.N1N2Message.Request.BinaryDataN2Information != nil {
			if requestData.N2InfoContainer.N2InformationClass == models.N2InformationClass_SM {
				targetPduSessionId = requestData.N2InfoContainer.SmInfo.PduSessionId
			} else {
				ue.N1N2Message = nil
				return fmt.Errorf("Service Request triggered by Network has not implemented about non SM N2Info")
			}
		}
	}

	if serviceRequest.UplinkDataStatus != nil {
		uplinkDataPsi := nasConvert.PSIToBooleanArray(serviceRequest.UplinkDataStatus.Buffer)
		reactivationResult = new([16]bool)
		ue.SmContextList.Range(func(key, value interface{}) bool {
			pduSessionID := key.(int32)
			smContext := value.(*context.SmContext)

			if pduSessionID != targetPduSessionId {
				if uplinkDataPsi[pduSessionID] && smContext.AccessType() == models.AccessType__3_GPP_ACCESS {
					response, errRes, _, err := consumer.SendUpdateSmContextActivateUpCnxState(
						ue, smContext, models.AccessType__3_GPP_ACCESS)
					if err != nil {
						ue.GmmLog.Errorf("SendUpdateSmContextActivateUpCnxState[pduSessionID:%d] Error: %+v",
							pduSessionID, err)
					} else if response == nil {
						reactivationResult[pduSessionID] = true
						errPduSessionId = append(errPduSessionId, uint8(pduSessionID))
						cause := nasMessage.Cause5GMMProtocolErrorUnspecified
						if errRes != nil {
							switch errRes.JsonData.Error.Cause {
							case "OUT_OF_LADN_SERVICE_AREA":
								cause = nasMessage.Cause5GMMLADNNotAvailable
							case "PRIORITIZED_SERVICES_ONLY":
								cause = nasMessage.Cause5GMMRestrictedServiceArea
							case "DNN_CONGESTION", "S-NSSAI_CONGESTION":
								cause = nasMessage.Cause5GMMInsufficientUserPlaneResourcesForThePDUSession
							}
						}
						errCause = append(errCause, cause)
					} else if ue.RanUe[anType].UeContextRequest {
						ngap_message.AppendPDUSessionResourceSetupListCxtReq(&ctxList,
							pduSessionID, smContext.Snssai(), nil, response.BinaryDataN2SmInformation)
					} else {
						ngap_message.AppendPDUSessionResourceSetupListSUReq(&suList,
							pduSessionID, smContext.Snssai(), nil, response.BinaryDataN2SmInformation)
					}
				}
			}
			return true
		})
	}
	if serviceRequest.PDUSessionStatus != nil {
		acceptPduSessionPsi = new([16]bool)
		psiArray := nasConvert.PSIToBooleanArray(serviceRequest.PDUSessionStatus.Buffer)
		ue.SmContextList.Range(func(key, value interface{}) bool {
			pduSessionID := key.(int32)
			smContext := value.(*context.SmContext)
			if smContext.AccessType() == anType {
				if !psiArray[pduSessionID] {
					cause := models.Cause_PDU_SESSION_STATUS_MISMATCH
					causeAll := &context.CauseAll{
						Cause: &cause,
					}
					problemDetail, err := consumer.SendReleaseSmContextRequest(ue, smContext, causeAll, "", nil)
					if problemDetail != nil {
						ue.GmmLog.Errorf("Release SmContext Failed Problem[%+v]", problemDetail)
					} else if err != nil {
						ue.GmmLog.Errorf("Release SmContext Error[%v]", err.Error())
					}
				} else {
					acceptPduSessionPsi[pduSessionID] = true
				}
			}
			return true
		})
	}
	switch serviceType {
	case nasMessage.ServiceTypeMobileTerminatedServices: // Trigger by Network
		if ue.N1N2Message != nil {
			requestData := ue.N1N2Message.Request.JsonData
			n1Msg := ue.N1N2Message.Request.BinaryDataN1Message
			n2Info := ue.N1N2Message.Request.BinaryDataN2Information

			// downlink signalling
			if n2Info == nil {
				err := sendServiceAccept(ue, anType, ctxList, suList, acceptPduSessionPsi,
					reactivationResult, errPduSessionId, errCause)
				if err != nil {
					return err
				}
				switch requestData.N1MessageContainer.N1MessageClass {
				case models.N1MessageClass_SM:
					gmm_message.SendDLNASTransport(ue.RanUe[anType],
						nasMessage.PayloadContainerTypeN1SMInfo, n1Msg, requestData.PduSessionId, 0, nil, 0)
				case models.N1MessageClass_LPP:
					gmm_message.SendDLNASTransport(ue.RanUe[anType], nasMessage.PayloadContainerTypeLPP, n1Msg, 0, 0, nil, 0)
				case models.N1MessageClass_SMS:
					gmm_message.SendDLNASTransport(ue.RanUe[anType], nasMessage.PayloadContainerTypeSMS, n1Msg, 0, 0, nil, 0)
				case models.N1MessageClass_UPDP:
					gmm_message.SendDLNASTransport(ue.RanUe[anType], nasMessage.PayloadContainerTypeUEPolicy, n1Msg, 0, 0, nil, 0)
				}
				ue.N1N2Message = nil
				return nil
			}
			// TODO: Area of validity for the N2 SM information
			smInfo := requestData.N2InfoContainer.SmInfo
			smContext, exist := ue.SmContextFindByPDUSessionID(requestData.PduSessionId)
			if !exist {
				ue.N1N2Message = nil
				return fmt.Errorf("Service Request triggered by Network error for pduSessionId does not exist")
			}

			if smContext.AccessType() == models.AccessType_NON_3_GPP_ACCESS {
				if serviceRequest.AllowedPDUSessionStatus != nil {
					allowPduSessionPsi := nasConvert.PSIToBooleanArray(serviceRequest.AllowedPDUSessionStatus.Buffer)
					if reactivationResult == nil {
						reactivationResult = new([16]bool)
					}
					if allowPduSessionPsi[requestData.PduSessionId] {
						response, errRes, _, err := consumer.SendUpdateSmContextChangeAccessType(
							ue, smContext, true)
						if err != nil {
							return err
						} else if response == nil {
							reactivationResult[requestData.PduSessionId] = true
							errPduSessionId = append(errPduSessionId, uint8(requestData.PduSessionId))
							cause := nasMessage.Cause5GMMProtocolErrorUnspecified
							if errRes != nil {
								switch errRes.JsonData.Error.Cause {
								case "OUT_OF_LADN_SERVICE_AREA":
									cause = nasMessage.Cause5GMMLADNNotAvailable
								case "PRIORITIZED_SERVICES_ONLY":
									cause = nasMessage.Cause5GMMRestrictedServiceArea
								case "DNN_CONGESTION", "S-NSSAI_CONGESTION":
									cause = nasMessage.Cause5GMMInsufficientUserPlaneResourcesForThePDUSession
								}
							}
							errCause = append(errCause, cause)
						} else {
							smContext.SetUserLocation(deepcopy.Copy(ue.Location).(models.UserLocation))
							smContext.SetAccessType(models.AccessType__3_GPP_ACCESS)
							if response.BinaryDataN2SmInformation != nil &&
								response.JsonData.N2SmInfoType == models.N2SmInfoType_PDU_RES_SETUP_REQ {
								if ue.RanUe[anType].UeContextRequest {
									ngap_message.AppendPDUSessionResourceSetupListCxtReq(&ctxList,
										requestData.PduSessionId, smContext.Snssai(), nil, response.BinaryDataN2SmInformation)
								} else {
									ngap_message.AppendPDUSessionResourceSetupListSUReq(&suList,
										requestData.PduSessionId, smContext.Snssai(), nil, response.BinaryDataN2SmInformation)
								}
							}
						}
					} else {
						ue.GmmLog.Warnf("UE was reachable but did not accept to re-activate the PDU Session[%d]",
							requestData.PduSessionId)
						callback.SendN1N2TransferFailureNotification(ue, models.N1N2MessageTransferCause_UE_NOT_REACHABLE_FOR_SESSION)
					}
				}
			} else if smInfo.N2InfoContent.NgapIeType == models.NgapIeType_PDU_RES_SETUP_REQ {
				var nasPdu []byte
				var err error
				if n1Msg != nil {
					pduSessionId := uint8(smInfo.PduSessionId)
					nasPdu, err = gmm_message.BuildDLNASTransport(ue, nasMessage.PayloadContainerTypeN1SMInfo,
						n1Msg, pduSessionId, nil, nil, 0)
					if err != nil {
						return err
					}
				}
				if ue.RanUe[anType].UeContextRequest {
					ngap_message.AppendPDUSessionResourceSetupListCxtReq(&ctxList, smInfo.PduSessionId, *smInfo.SNssai, nasPdu, n2Info)
				} else {
					ngap_message.AppendPDUSessionResourceSetupListSUReq(&suList, smInfo.PduSessionId, *smInfo.SNssai,
						nasPdu, n2Info)
				}
			}
			err := sendServiceAccept(ue, anType, ctxList, suList, acceptPduSessionPsi,
				reactivationResult, errPduSessionId, errCause)
			if err != nil {
				return err
			}
		}
		// downlink signaling
		if ue.ConfigurationUpdateMessage != nil {
			err := sendServiceAccept(ue, anType, ctxList, suList,
				acceptPduSessionPsi, reactivationResult, errPduSessionId, errCause)
			if err != nil {
				return err
			}
			mobilityRestrictionList := ngap_message.BuildIEMobilityRestrictionList(ue)
			ngap_message.SendDownlinkNasTransport(ue.RanUe[models.AccessType__3_GPP_ACCESS],
				ue.ConfigurationUpdateMessage, &mobilityRestrictionList)
			ue.ConfigurationUpdateMessage = nil
		}
	case nasMessage.ServiceTypeData:
		if anType == models.AccessType__3_GPP_ACCESS {
			if ue.AmPolicyAssociation != nil && ue.AmPolicyAssociation.ServAreaRes != nil {
				var accept bool
				switch ue.AmPolicyAssociation.ServAreaRes.RestrictionType {
				case models.RestrictionType_ALLOWED_AREAS:
					accept = context.TacInAreas(ue.Tai.Tac, ue.AmPolicyAssociation.ServAreaRes.Areas)
				case models.RestrictionType_NOT_ALLOWED_AREAS:
					accept = !context.TacInAreas(ue.Tai.Tac, ue.AmPolicyAssociation.ServAreaRes.Areas)
				}

				if !accept {
					gmm_message.SendServiceReject(ue.RanUe[anType], nil, nasMessage.Cause5GMMRestrictedServiceArea)
					return nil
				}
			}
			err := sendServiceAccept(ue, anType, ctxList, suList, acceptPduSessionPsi,
				reactivationResult, errPduSessionId, errCause)
			if err != nil {
				return err
			}
		} else {
			err := sendServiceAccept(ue, anType, ctxList, suList, acceptPduSessionPsi,
				reactivationResult, errPduSessionId, errCause)
			if err != nil {
				return err
			}
		}
	default:
		return fmt.Errorf("Service Type[%d] is not supported", serviceType)
	}
	if len(errPduSessionId) != 0 {
		ue.GmmLog.Info(errPduSessionId, errCause)
	}
	ue.N1N2Message = nil
	return nil
}

func sendServiceAccept(ue *context.AmfUe, anType models.AccessType, ctxList ngapType.PDUSessionResourceSetupListCxtReq,
	suList ngapType.PDUSessionResourceSetupListSUReq, pDUSessionStatus *[16]bool,
	reactivationResult *[16]bool, errPduSessionId, errCause []uint8) error {
	if ue.RanUe[anType].UeContextRequest {
		// update Kgnb/Kn3iwf
		ue.UpdateSecurityContext(anType)

		nasPdu, err := gmm_message.BuildServiceAccept(ue, pDUSessionStatus, reactivationResult,
			errPduSessionId, errCause)
		if err != nil {
			return err
		}
		if len(ctxList.List) != 0 {
			ngap_message.SendInitialContextSetupRequest(ue, anType, nasPdu, &ctxList, nil, nil, nil)
		} else {
			ngap_message.SendInitialContextSetupRequest(ue, anType, nasPdu, nil, nil, nil, nil)
		}
	} else if len(suList.List) != 0 {
		nasPdu, err := gmm_message.BuildServiceAccept(ue, pDUSessionStatus, reactivationResult,
			errPduSessionId, errCause)
		if err != nil {
			return err
		}
		ngap_message.SendPDUSessionResourceSetupRequest(ue.RanUe[anType], nasPdu, suList)
	} else {
		gmm_message.SendServiceAccept(ue.RanUe[anType], pDUSessionStatus, reactivationResult, errPduSessionId, errCause)
	}
	return nil
}

// TS 24.501 5.4.1
func HandleAuthenticationResponse(ue *context.AmfUe, accessType models.AccessType,
	authenticationResponse *nasMessage.AuthenticationResponse) error {
	ue.GmmLog.Info("Handle Authentication Response")

	if ue.T3560 != nil {
		ue.T3560.Stop()
		ue.T3560 = nil // clear the timer
	}

	if ue.AuthenticationCtx == nil {
		return fmt.Errorf("Ue Authentication Context is nil")
	}

	switch ue.AuthenticationCtx.AuthType {
	case models.AuthType__5_G_AKA:
		var av5gAka models.Av5gAka
		if err := mapstructure.Decode(ue.AuthenticationCtx.Var5gAuthData, &av5gAka); err != nil {
			return fmt.Errorf("Var5gAuthData Convert Type Error")
		}
		resStar := authenticationResponse.AuthenticationResponseParameter.GetRES()

		// Calculate HRES* (TS 33.501 Annex A.5)
		p0, err := hex.DecodeString(av5gAka.Rand)
		if err != nil {
			return err
		}
		p1 := resStar[:]
		concat := append(p0, p1...)
		hResStarBytes := sha256.Sum256(concat)
		hResStar := hex.EncodeToString(hResStarBytes[16:])

		if hResStar != av5gAka.HxresStar {
			ue.GmmLog.Errorf("HRES* Validation Failure (received: %s, expected: %s)", hResStar, av5gAka.HxresStar)

			if ue.IdentityTypeUsedForRegistration == nasMessage.MobileIdentity5GSType5gGuti {
				gmm_message.SendIdentityRequest(ue.RanUe[accessType], nasMessage.MobileIdentity5GSTypeSuci)
				return nil
			} else {
				gmm_message.SendAuthenticationReject(ue.RanUe[accessType], "")
				return GmmFSM.SendEvent(ue.State[accessType], AuthFailEvent, fsm.ArgsType{
					ArgAmfUe:      ue,
					ArgAccessType: accessType,
				})
			}
		}

		// By uuuuu - entry
		// 原为 consumer.SendAuth5gAkaConfirmRequest
		// 现将 AUSF comfirm 流程置于此，比较此前 auth 生成的 xres 与收到的 res
		updateConfirmationData := models.ConfirmationData{hex.EncodeToString(resStar[:])}
		//fmt.Printf("res*: %x\nXres*: %x\n", updateConfirmationData.ResStar, xresForConfirm[ue.Supi])

		if strings.Compare(updateConfirmationData.ResStar, xresForConfirm[ue.Supi]) == 0 {
			ausf_logger.Auth5gAkaComfirmLog.Infof("5G AKA confirmation succeeded")
		} else {
			ausf_logger.Auth5gAkaComfirmLog.Infof("5G AKA confirmation failed")
		}

		// 下面去掉 sendAuthResultToUDM(currentSupi, models.AuthType__5_G_AKA, success, servingNetworkName, ausfCurrentContext.UdmUeauUrl)
		// 原本会在 mongo 里写入一条 "subscriptionData.authenticationData.authenticationStatus"

		ue.UnauthenticatedSupi = false
		ue.Kseaf = hex.EncodeToString(Kseaf[ue.Supi])

		ue.DerivateKamf()
		ue.GmmLog.Debugln("ue.DerivateKamf()", ue.Kamf)
		return GmmFSM.SendEvent(ue.State[accessType], AuthSuccessEvent, fsm.ArgsType{
			ArgAmfUe:      ue,
			ArgAccessType: accessType,
			ArgEAPSuccess: false,
			ArgEAPMessage: "",
		})
	case models.AuthType_EAP_AKA_PRIME:
		fmt.Println("EAP not done!")
	}

	// By uuuuu - end

	return nil
}

func HandleAuthenticationFailure(ue *context.AmfUe, anType models.AccessType,
	authenticationFailure *nasMessage.AuthenticationFailure) error {
	ue.GmmLog.Info("Handle Authentication Failure")

	if ue.T3560 != nil {
		ue.T3560.Stop()
		ue.T3560 = nil // clear the timer
	}

	cause5GMM := authenticationFailure.Cause5GMM.GetCauseValue()

	if ue.AuthenticationCtx.AuthType == models.AuthType__5_G_AKA {
		switch cause5GMM {
		case nasMessage.Cause5GMMMACFailure:
			ue.GmmLog.Warnln("Authentication Failure Cause: Mac Failure")
			gmm_message.SendAuthenticationReject(ue.RanUe[anType], "")
			return GmmFSM.SendEvent(ue.State[anType], AuthFailEvent, fsm.ArgsType{ArgAmfUe: ue, ArgAccessType: anType})
		case nasMessage.Cause5GMMNon5GAuthenticationUnacceptable:
			ue.GmmLog.Warnln("Authentication Failure Cause: Non-5G Authentication Unacceptable")
			gmm_message.SendAuthenticationReject(ue.RanUe[anType], "")
			return GmmFSM.SendEvent(ue.State[anType], AuthFailEvent, fsm.ArgsType{ArgAmfUe: ue, ArgAccessType: anType})
		case nasMessage.Cause5GMMngKSIAlreadyInUse:
			ue.GmmLog.Warnln("Authentication Failure Cause: NgKSI Already In Use")
			ue.AuthFailureCauseSynchFailureTimes = 0
			ue.GmmLog.Warnln("Select new NgKsi")
			// select new ngksi
			if ue.NgKsi.Ksi < 6 { // ksi is range from 0 to 6
				ue.NgKsi.Ksi += 1
			} else {
				ue.NgKsi.Ksi = 0
			}
			gmm_message.SendAuthenticationRequest(ue.RanUe[anType])
		case nasMessage.Cause5GMMSynchFailure: // TS 24.501 5.4.1.3.7 case f
			ue.GmmLog.Warn("Authentication Failure 5GMM Cause: Synch Failure")

			ue.AuthFailureCauseSynchFailureTimes++
			if ue.AuthFailureCauseSynchFailureTimes >= 5 {
				ue.GmmLog.Warnf("5 consecutive Synch Failure, terminate authentication procedure")
				gmm_message.SendAuthenticationReject(ue.RanUe[anType], "")
				return GmmFSM.SendEvent(ue.State[anType], AuthFailEvent, fsm.ArgsType{ArgAmfUe: ue, ArgAccessType: anType})
			}

			auts := authenticationFailure.AuthenticationFailureParameter.GetAuthenticationFailureParameter()
			resynchronizationInfo := &models.ResynchronizationInfo{
				Auts: hex.EncodeToString(auts[:]),
			}
			
			// By uuuuu - entry
			// rebuild ue.authRequest

			amfSelf := context.AMF_Self()
			servedGuami := amfSelf.ServedGuamiList[0]
		
			var authInfo models.AuthenticationInfo
			authInfo.SupiOrSuci = ue.Suci
		
			mnc, _ := strconv.Atoi(servedGuami.PlmnId.Mnc)
			authInfo.ServingNetworkName = fmt.Sprintf("5G:mnc%03d.mcc%s.3gppnetwork.org", mnc, servedGuami.PlmnId.Mcc)
		
			RAND := mapForSqn[ue.Supi].rand
		
			opc := mapForSqn[ue.Supi].opc
			AMF, _ := hex.DecodeString("8000")
			//fmt.Println("\n\nauts: ",resynchronizationInfo.Auts)
			gotAuts, _ := hex.DecodeString(resynchronizationInfo.Auts)
			sqn, macS := aucSQN(mapForSqn[ue.Supi].opc, mapForSqn[ue.Supi].k, gotAuts, mapForSqn[ue.Supi].rand)

			k:=mapForSqn[ue.Supi].k

			bigSQN := big.NewInt(0)
			sqnStr := hex.EncodeToString(sqn)
			//fmt.Printf("SQNstr %s\n", sqnStr)
			bigSQN.SetString(sqnStr, 16)

			bigInc := big.NewInt(33)

			bigP := big.NewInt(SqnMAx)
			bigSQN = bigInc.Add(bigSQN, bigInc)
			bigSQN = bigSQN.Mod(bigSQN, bigP)
			sqnStr = fmt.Sprintf("%x", bigSQN)
			sqnStr = strictHex(sqnStr, 12)

			bigSQN = big.NewInt(0)
			sqn, _ = hex.DecodeString(sqnStr)

			bigSQN.SetString(sqnStr, 16)

			bigInc = big.NewInt(1)
			bigSQN = bigInc.Add(bigSQN, bigInc)

			SQNheStr := fmt.Sprintf("%x", bigSQN)
			SQNheStr = strictHex(SQNheStr, 12)

			macA, macS := make([]byte, 8), make([]byte, 8)
			CK, IK := make([]byte, 16), make([]byte, 16)
			RES := make([]byte, 8)
			AK, AKstar := make([]byte, 6), make([]byte, 6)

			// Generate macA, macS
			milenage.F1(opc, k, RAND, sqn, AMF, macA, macS)
			milenage.F2345(opc, k, RAND, RES, CK, IK, AK, AKstar)
	
			SQNxorAK := make([]byte, 6)
			for i := 0; i < len(sqn); i++ {
				SQNxorAK[i] = sqn[i] ^ AK[i]
			}
			// fmt.Printf("SQN xor AK = %x\n", SQNxorAK)
			AUTN := append(append(SQNxorAK, AMF...), macA...)
			//fmt.Printf("\nnew AUTN = %x\n", AUTN)
		
			var av models.AuthenticationVector
			var am models.AuthType

			am = models.AuthType__5_G_AKA
			// derive XRES*
			key := append(CK, IK...)
			FC := UeauCommon.FC_FOR_RES_STAR_XRES_STAR_DERIVATION
			P0 := []byte(authInfo.ServingNetworkName)
			P1 := RAND
			P2 := RES
		
			kdfValForXresStar := UeauCommon.GetKDFValue(
				key, FC, P0, UeauCommon.KDFLen(P0), P1, UeauCommon.KDFLen(P1), P2, UeauCommon.KDFLen(P2))
			xresStar := kdfValForXresStar[len(kdfValForXresStar)/2:]
		
			// derive Kausf
			FC = UeauCommon.FC_FOR_KAUSF_DERIVATION
			P0 = []byte(authInfo.ServingNetworkName)
			P1 = SQNxorAK
			kdfValForKausf := UeauCommon.GetKDFValue(key, FC, P0, UeauCommon.KDFLen(P0), P1, UeauCommon.KDFLen(P1))
			// fmt.Printf("Kausf = %x\n", kdfValForKausf)
		
			// Fill in rand, xresStar, autn, kausf
			av.Rand = hex.EncodeToString(RAND)
			av.XresStar = hex.EncodeToString(xresStar)
			av.Autn = hex.EncodeToString(AUTN)
			av.Kausf = hex.EncodeToString(kdfValForKausf)
			
			// UDM 部分结束，进入 ausf/producer/ue_authentication.go 开始照搬
			var response models.UeAuthenticationCtx
			response.ServingNetworkName = authInfo.ServingNetworkName

			// Derive HXRES* from XRES*
			concat := av.Rand + av.XresStar
			var hxresStarBytes []byte
		
			bytes, _ := hex.DecodeString(concat)
			hxresStarBytes = bytes
			hxresStarAll := sha256.Sum256(hxresStarBytes)
			hxresStar := hex.EncodeToString(hxresStarAll[16:]) // last 128 bits
		
			// Derive Kseaf from Kausf
			Kausf := av.Kausf
			var KausfDecode []byte
			ausfDecode, _ := hex.DecodeString(Kausf)
			KausfDecode = ausfDecode
		
			P0 = []byte(authInfo.ServingNetworkName)
		
			Kseaf[ue.Supi] = UeauCommon.GetKDFValue(KausfDecode, UeauCommon.FC_FOR_KSEAF_DERIVATION, P0, UeauCommon.KDFLen(P0))
		
			var av5gAka models.Av5gAka
			av5gAka.Rand = av.Rand
			av5gAka.Autn = av.Autn
			av5gAka.HxresStar = hxresStar
		
			response.Var5gAuthData = av5gAka

			response.AuthType = am
		
			// 自己加了个全局 map，取代 ausfcontext 来完成 xres 的比较
			xresForConfirm[ue.Supi] = av.XresStar
			ue.AuthenticationCtx = &response
			ue.ABBA = []uint8{0x00, 0x00} // set ABBA value as described at TS 33.501 Annex A.7.1

			// response, problemDetails, err := consumer.SendUEAuthenticationAuthenticateRequest(ue, resynchronizationInfo)
			// if err != nil {
			// 	return err
			// } else if problemDetails != nil {
			// 	ue.GmmLog.Errorf("Nausf_UEAU Authenticate Request Error[Problem Detail: %+v]", problemDetails)
			// 	return nil
			// }
			// ue.AuthenticationCtx = response
			// ue.ABBA = []uint8{0x00, 0x00}

			// By uuuuu - end

			gmm_message.SendAuthenticationRequest(ue.RanUe[anType])
		}
	} else if ue.AuthenticationCtx.AuthType == models.AuthType_EAP_AKA_PRIME {
		switch cause5GMM {
		case nasMessage.Cause5GMMngKSIAlreadyInUse:
			ue.GmmLog.Warn("Authentication Failure 5GMM Cause: NgKSI Already In Use")
			if ue.NgKsi.Ksi < 6 { // ksi is range from 0 to 6
				ue.NgKsi.Ksi += 1
			} else {
				ue.NgKsi.Ksi = 0
			}
			gmm_message.SendAuthenticationRequest(ue.RanUe[anType])
		}
	}

	return nil
}

func HandleRegistrationComplete(ue *context.AmfUe, accessType models.AccessType,
	registrationComplete *nasMessage.RegistrationComplete) error {
	ue.GmmLog.Info("Handle Registration Complete")

	if ue.T3550 != nil {
		ue.T3550.Stop()
		ue.T3550 = nil // clear the timer
	}

	// if registrationComplete.SORTransparentContainer != nil {
	// 	TODO: if at regsitration procedure 14b, udm provide amf Steering of Roaming info & request an ack,
	// 	AMF provides the UE's ack with Nudm_SDM_Info (SOR not supportted in this stage)
	// }

	// TODO: if
	//	1. AMF has evaluated the support of IMS Voice over PS Sessions (TS 23.501 5.16.3.2)
	//	2. AMF determines that it needs to update the Homogeneous Support of IMS Voice over PS Sessions (TS 23.501 5.16.3.3)
	// Then invoke Nudm_UECM_Update to send "Homogeneous Support of IMS Voice over PS Sessions" indication to udm

	if ue.RegistrationRequest.UplinkDataStatus == nil &&
		ue.RegistrationRequest.GetFOR() == nasMessage.FollowOnRequestNoPending {
		ngap_message.SendUEContextReleaseCommand(ue.RanUe[accessType], context.UeContextN2NormalRelease,
			ngapType.CausePresentNas, ngapType.CauseNasPresentNormalRelease)
	}
	return GmmFSM.SendEvent(ue.State[accessType], ContextSetupSuccessEvent, fsm.ArgsType{
		ArgAmfUe:      ue,
		ArgAccessType: accessType,
	})
}

// TS 33.501 6.7.2
func HandleSecurityModeComplete(ue *context.AmfUe, anType models.AccessType, procedureCode int64,
	securityModeComplete *nasMessage.SecurityModeComplete) error {
	ue.GmmLog.Info("Handle Security Mode Complete")
	if ue.MacFailed {
		return fmt.Errorf("NAS message integrity check failed")
	}

	if ue.T3560 != nil {
		ue.T3560.Stop()
		ue.T3560 = nil // clear the timer
	}

	if ue.SecurityContextIsValid() {
		// update Kgnb/Kn3iwf
		ue.UpdateSecurityContext(anType)
	}

	if securityModeComplete.IMEISV != nil {
		ue.GmmLog.Debugln("receieve IMEISV")
		ue.Pei = nasConvert.PeiToString(securityModeComplete.IMEISV.Octet[:])
	}
	// TODO: AMF shall set the NAS COUNTs to zero if horizontal derivation of KAMF is performed
	if securityModeComplete.NASMessageContainer != nil {
		contents := securityModeComplete.NASMessageContainer.GetNASMessageContainerContents()
		m := nas.NewMessage()
		if err := m.GmmMessageDecode(&contents); err != nil {
			return err
		}
		messageType := m.GmmMessage.GmmHeader.GetMessageType()
		if messageType != nas.MsgTypeRegistrationRequest && messageType != nas.MsgTypeServiceRequest {
			ue.GmmLog.Errorln("nas message container Iei type error")
			return errors.New("nas message container Iei type error")
		} else {
			return GmmFSM.SendEvent(ue.State[anType], SecurityModeSuccessEvent, fsm.ArgsType{
				ArgAmfUe:         ue,
				ArgAccessType:    anType,
				ArgProcedureCode: procedureCode,
				ArgNASMessage:    m.GmmMessage.RegistrationRequest,
			})
		}
	}
	return GmmFSM.SendEvent(ue.State[anType], SecurityModeSuccessEvent, fsm.ArgsType{
		ArgAmfUe:         ue,
		ArgAccessType:    anType,
		ArgProcedureCode: procedureCode,
		ArgNASMessage:    ue.RegistrationRequest,
	})
}

func HandleSecurityModeReject(ue *context.AmfUe, anType models.AccessType,
	securityModeReject *nasMessage.SecurityModeReject) error {
	ue.GmmLog.Info("Handle Security Mode Reject")

	if ue.T3560 != nil {
		ue.T3560.Stop()
		ue.T3560 = nil // clear the timer
	}

	cause := securityModeReject.Cause5GMM.GetCauseValue()
	ue.GmmLog.Warnf("Reject Cause: %s", nasMessage.Cause5GMMToString(cause))
	ue.GmmLog.Error("UE reject the security mode command, abort the ongoing procedure")
	return nil
}

// TS 23.502 4.2.2.3
func HandleDeregistrationRequest(ue *context.AmfUe, anType models.AccessType,
	deregistrationRequest *nasMessage.DeregistrationRequestUEOriginatingDeregistration) error {
	ue.GmmLog.Info("Handle Deregistration Request(UE Originating)")

	targetDeregistrationAccessType := deregistrationRequest.GetAccessType()
	ue.SmContextList.Range(func(key, value interface{}) bool {
		smContext := value.(*context.SmContext)

		if smContext.AccessType() == anType ||
			targetDeregistrationAccessType == nasMessage.AccessTypeBoth {
			problemDetail, err := consumer.SendReleaseSmContextRequest(ue, smContext, nil, "", nil)
			if problemDetail != nil {
				ue.GmmLog.Errorf("Release SmContext Failed Problem[%+v]", problemDetail)
			} else if err != nil {
				ue.GmmLog.Errorf("Release SmContext Error[%v]", err.Error())
			}
		}
		return true
	})

	if ue.AmPolicyAssociation != nil {
		terminateAmPolicyAssocaition := true
		switch anType {
		case models.AccessType__3_GPP_ACCESS:
			terminateAmPolicyAssocaition = ue.State[models.AccessType_NON_3_GPP_ACCESS].Is(context.Deregistered)
		case models.AccessType_NON_3_GPP_ACCESS:
			terminateAmPolicyAssocaition = ue.State[models.AccessType__3_GPP_ACCESS].Is(context.Deregistered)
		}

		if terminateAmPolicyAssocaition {
			problemDetails, err := consumer.AMPolicyControlDelete(ue)
			if problemDetails != nil {
				ue.GmmLog.Errorf("AM Policy Control Delete Failed Problem[%+v]", problemDetails)
			} else if err != nil {
				ue.GmmLog.Errorf("AM Policy Control Delete Error[%v]", err.Error())
			}
		}
	}

	// if Deregistration type is not switch-off, send Deregistration Accept
	if deregistrationRequest.GetSwitchOff() == 0 {
		gmm_message.SendDeregistrationAccept(ue.RanUe[anType])
	}

	// TS 23.502 4.2.6, 4.12.3
	switch targetDeregistrationAccessType {
	case nasMessage.AccessType3GPP:
		if ue.RanUe[models.AccessType__3_GPP_ACCESS] != nil {
			ngap_message.SendUEContextReleaseCommand(ue.RanUe[models.AccessType__3_GPP_ACCESS],
				context.UeContextReleaseUeContext, ngapType.CausePresentNas, ngapType.CauseNasPresentDeregister)
		}
		return GmmFSM.SendEvent(ue.State[models.AccessType__3_GPP_ACCESS], DeregistrationAcceptEvent, fsm.ArgsType{
			ArgAmfUe:      ue,
			ArgAccessType: anType,
		})
	case nasMessage.AccessTypeNon3GPP:
		if ue.RanUe[models.AccessType_NON_3_GPP_ACCESS] != nil {
			ngap_message.SendUEContextReleaseCommand(ue.RanUe[models.AccessType_NON_3_GPP_ACCESS],
				context.UeContextReleaseUeContext, ngapType.CausePresentNas, ngapType.CauseNasPresentDeregister)
		}
		return GmmFSM.SendEvent(ue.State[models.AccessType_NON_3_GPP_ACCESS], DeregistrationAcceptEvent, fsm.ArgsType{
			ArgAmfUe:      ue,
			ArgAccessType: anType,
		})
	case nasMessage.AccessTypeBoth:
		if ue.RanUe[models.AccessType__3_GPP_ACCESS] != nil {
			ngap_message.SendUEContextReleaseCommand(ue.RanUe[models.AccessType__3_GPP_ACCESS],
				context.UeContextReleaseUeContext, ngapType.CausePresentNas, ngapType.CauseNasPresentDeregister)
		}
		if ue.RanUe[models.AccessType_NON_3_GPP_ACCESS] != nil {
			ngap_message.SendUEContextReleaseCommand(ue.RanUe[models.AccessType_NON_3_GPP_ACCESS],
				context.UeContextReleaseUeContext, ngapType.CausePresentNas, ngapType.CauseNasPresentDeregister)
		}

		err := GmmFSM.SendEvent(ue.State[models.AccessType__3_GPP_ACCESS], DeregistrationAcceptEvent, fsm.ArgsType{
			ArgAmfUe:      ue,
			ArgAccessType: anType,
		})
		if err != nil {
			ue.GmmLog.Errorln(err)
		}
		return GmmFSM.SendEvent(ue.State[models.AccessType_NON_3_GPP_ACCESS], DeregistrationAcceptEvent, fsm.ArgsType{
			ArgAmfUe:      ue,
			ArgAccessType: anType,
		})
	}

	return nil
}

// TS 23.502 4.2.2.3
func HandleDeregistrationAccept(ue *context.AmfUe, anType models.AccessType,
	deregistrationAccept *nasMessage.DeregistrationAcceptUETerminatedDeregistration) error {
	ue.GmmLog.Info("Handle Deregistration Accept(UE Terminated)")

	if ue.T3522 != nil {
		ue.T3522.Stop()
		ue.T3522 = nil // clear the timer
	}

	switch ue.DeregistrationTargetAccessType {
	case nasMessage.AccessType3GPP:
		if ue.RanUe[models.AccessType__3_GPP_ACCESS] != nil {
			ngap_message.SendUEContextReleaseCommand(ue.RanUe[models.AccessType__3_GPP_ACCESS],
				context.UeContextReleaseUeContext, ngapType.CausePresentNas, ngapType.CauseNasPresentDeregister)
		}
	case nasMessage.AccessTypeNon3GPP:
		if ue.RanUe[models.AccessType_NON_3_GPP_ACCESS] != nil {
			ngap_message.SendUEContextReleaseCommand(ue.RanUe[models.AccessType_NON_3_GPP_ACCESS],
				context.UeContextReleaseUeContext, ngapType.CausePresentNas, ngapType.CauseNasPresentDeregister)
		}
	case nasMessage.AccessTypeBoth:
		if ue.RanUe[models.AccessType__3_GPP_ACCESS] != nil {
			ngap_message.SendUEContextReleaseCommand(ue.RanUe[models.AccessType__3_GPP_ACCESS],
				context.UeContextReleaseUeContext, ngapType.CausePresentNas, ngapType.CauseNasPresentDeregister)
		}
		if ue.RanUe[models.AccessType_NON_3_GPP_ACCESS] != nil {
			ngap_message.SendUEContextReleaseCommand(ue.RanUe[models.AccessType_NON_3_GPP_ACCESS],
				context.UeContextReleaseUeContext, ngapType.CausePresentNas, ngapType.CauseNasPresentDeregister)
		}
	}

	ue.DeregistrationTargetAccessType = 0
	return nil
}

func HandleStatus5GMM(ue *context.AmfUe, anType models.AccessType, status5GMM *nasMessage.Status5GMM) error {
	ue.GmmLog.Info("Handle Staus 5GMM")
	if ue.MacFailed {
		return fmt.Errorf("NAS message integrity check failed")
	}

	cause := status5GMM.Cause5GMM.GetCauseValue()
	ue.GmmLog.Errorf("Error condition [Cause Value: %s]", nasMessage.Cause5GMMToString(cause))
	return nil
}

func aucSQN(opc, k, auts, rand []byte) ([]byte, []byte) {
	AK, SQNms := make([]byte, 6), make([]byte, 6)
	macS := make([]byte, 8)
	ConcSQNms := auts[:6]
	AMF, err := hex.DecodeString("0000")
	if err != nil {
		return nil, nil
	}

	milenage.F2345(opc, k, rand, nil, nil, nil, nil, AK)

	for i := 0; i < 6; i++ {
		SQNms[i] = AK[i] ^ ConcSQNms[i]
	}

	milenage.F1(opc, k, rand, SQNms, AMF, nil, macS)

	//fmt.Println("SQNms", SQNms)
	//fmt.Println("\n\nmacS", hex.EncodeToString(macS[:]))
	return SQNms, macS
}